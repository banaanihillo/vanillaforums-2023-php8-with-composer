<?php return array (
  'dashboard' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'description' => 'Handles user, role, permission, plugin, theme, and application management.',
      'allowDisable' => false,
      'url' => 'https://open.vanillaforums.com',
      'license' => 'GPL-2.0-only',
      'priority' => 5,
      'hidden' => true,
      'icon' => 'dashboard.png',
      'key' => 'dashboard',
      'type' => 'addon',
      'name' => 'Dashboard',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Vanilla Staff',
          'email' => 'support@vanillaforums.com',
          'homepage' => 'https://open.vanillaforums.com',
        ),
      ),
      'version' => '3.0',
      'oldType' => 'application',
      'keyRaw' => 'Dashboard',
      'Issues' => 
      array (
      ),
    ),
     'classes' => 
    array (
      'socialcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SocialController',
         'filePath' => 'applications/dashboard/controllers/class.socialcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'notificationscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NotificationsController',
         'filePath' => 'applications/dashboard/controllers/class.notificationscontroller.php',
         'addonKey' => 'dashboard',
      )),
      'utilitycontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UtilityController',
         'filePath' => 'applications/dashboard/controllers/class.utilitycontroller.php',
         'addonKey' => 'dashboard',
      )),
      'staticcontentcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'StaticContentController',
         'filePath' => 'applications/dashboard/controllers/StaticContentController.php',
         'addonKey' => 'dashboard',
      )),
      'managecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ManageController',
         'filePath' => 'applications/dashboard/controllers/ManageController.php',
         'addonKey' => 'dashboard',
      )),
      'rolecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RoleController',
         'filePath' => 'applications/dashboard/controllers/class.rolecontroller.php',
         'addonKey' => 'dashboard',
      )),
      'entrycontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'EntryController',
         'filePath' => 'applications/dashboard/controllers/class.entrycontroller.php',
         'addonKey' => 'dashboard',
      )),
      'sessioncontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SessionController',
         'filePath' => 'applications/dashboard/controllers/class.sessioncontroller.php',
         'addonKey' => 'dashboard',
      )),
      'robotscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RobotsController',
         'filePath' => 'applications/dashboard/controllers/RobotsController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\layoutsettingspagecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\LayoutSettingsPageController',
         'filePath' => 'applications/dashboard/controllers/LayoutSettingsPageController.php',
         'addonKey' => 'dashboard',
      )),
      'activitycontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActivityController',
         'filePath' => 'applications/dashboard/controllers/class.activitycontroller.php',
         'addonKey' => 'dashboard',
      )),
      'settingscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SettingsController',
         'filePath' => 'applications/dashboard/controllers/class.settingscontroller.php',
         'addonKey' => 'dashboard',
      )),
      'homecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'HomeController',
         'filePath' => 'applications/dashboard/controllers/class.homecontroller.php',
         'addonKey' => 'dashboard',
      )),
      'embedcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'EmbedController',
         'filePath' => 'applications/dashboard/controllers/class.embedcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'routescontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RoutesController',
         'filePath' => 'applications/dashboard/controllers/class.routescontroller.php',
         'addonKey' => 'dashboard',
      )),
      'requestscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RequestsController',
         'filePath' => 'applications/dashboard/controllers/RequestsController.php',
         'addonKey' => 'dashboard',
      )),
      'setupcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SetupController',
         'filePath' => 'applications/dashboard/controllers/class.setupcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'tickapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TickApiController',
         'filePath' => 'applications/dashboard/controllers/TickApiController.php',
         'addonKey' => 'dashboard',
      )),
      'messagecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MessageController',
         'filePath' => 'applications/dashboard/controllers/class.messagecontroller.php',
         'addonKey' => 'dashboard',
      )),
      'importcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ImportController',
         'filePath' => 'applications/dashboard/controllers/class.importcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'assetcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AssetController',
         'filePath' => 'applications/dashboard/controllers/class.assetcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'dashboardcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DashboardController',
         'filePath' => 'applications/dashboard/controllers/class.dashboardcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'rootcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RootController',
         'filePath' => 'applications/dashboard/controllers/class.rootcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'usercontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserController',
         'filePath' => 'applications/dashboard/controllers/class.usercontroller.php',
         'addonKey' => 'dashboard',
      )),
      'addoncachecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AddonCacheController',
         'filePath' => 'applications/dashboard/controllers/class.addoncachecontroller.php',
         'addonKey' => 'dashboard',
      )),
      'dbacontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DbaController',
         'filePath' => 'applications/dashboard/controllers/class.dbacontroller.php',
         'addonKey' => 'dashboard',
      )),
      'statisticscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'StatisticsController',
         'filePath' => 'applications/dashboard/controllers/class.statisticscontroller.php',
         'addonKey' => 'dashboard',
      )),
      'searchcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SearchController',
         'filePath' => 'applications/dashboard/controllers/class.searchcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'modulecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ModuleController',
         'filePath' => 'applications/dashboard/controllers/class.modulecontroller.php',
         'addonKey' => 'dashboard',
      )),
      'profilecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ProfileController',
         'filePath' => 'applications/dashboard/controllers/class.profilecontroller.php',
         'addonKey' => 'dashboard',
      )),
      'plugincontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PluginController',
         'filePath' => 'applications/dashboard/controllers/class.plugincontroller.php',
         'addonKey' => 'dashboard',
      )),
      'logcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LogController',
         'filePath' => 'applications/dashboard/controllers/class.logcontroller.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\pages\\homepagecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Pages\\HomePageController',
         'filePath' => 'applications/dashboard/controllers/Pages/HomePageController.php',
         'addonKey' => 'dashboard',
      )),
      'dashboardpage' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DashboardPage',
         'filePath' => 'applications/dashboard/controllers/Pages/DashboardPage.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\pages\\appearancepagecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Pages\\AppearancePageController',
         'filePath' => 'applications/dashboard/controllers/Pages/AppearancePageController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\pages\\legacydashboardpage' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Pages\\LegacyDashboardPage',
         'filePath' => 'applications/dashboard/controllers/Pages/LegacyDashboardPage.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\layoutsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\API\\LayoutsApiController',
         'filePath' => 'applications/dashboard/controllers/api/LayoutsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\openapiapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\API\\OpenApiApiController',
         'filePath' => 'applications/dashboard/controllers/api/OpenApiApiController.php',
         'addonKey' => 'dashboard',
      )),
      'themesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ThemesApiController',
         'filePath' => 'applications/dashboard/controllers/api/ThemesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\controllers\\api\\searchapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Controllers\\Api\\SearchApiController',
         'filePath' => 'applications/dashboard/controllers/api/SearchApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\assetsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\AssetsApiController',
         'filePath' => 'applications/dashboard/controllers/api/AssetsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\jobstatusesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\JobStatusesApiController',
         'filePath' => 'applications/dashboard/controllers/api/JobStatusesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'abstractapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AbstractApiController',
         'filePath' => 'applications/dashboard/controllers/api/AbstractApiController.php',
         'addonKey' => 'dashboard',
      )),
      'mediaapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MediaApiController',
         'filePath' => 'applications/dashboard/controllers/api/MediaApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\api\\dashboardapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Api\\DashboardApiController',
         'filePath' => 'applications/dashboard/controllers/api/DashboardApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\sitetotalsfilteropenapi' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\SiteTotalsFilterOpenApi',
         'filePath' => 'applications/dashboard/controllers/api/SiteTotalsFilterOpenApi.php',
         'addonKey' => 'dashboard',
      )),
      'authenticatorsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AuthenticatorsApiController',
         'filePath' => 'applications/dashboard/controllers/api/AuthenticatorsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\configapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\API\\ConfigApiController',
         'filePath' => 'applications/dashboard/controllers/api/ConfigApiController.php',
         'addonKey' => 'dashboard',
      )),
      'localesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LocalesApiController',
         'filePath' => 'applications/dashboard/controllers/api/LocalesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'sourcemapsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SourcemapsApiController',
         'filePath' => 'applications/dashboard/controllers/api/SourcemapsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\sitetotalsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\SiteTotalsApiController',
         'filePath' => 'applications/dashboard/controllers/api/SiteTotalsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controller\\api\\usermentionsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\controller\\api\\UserMentionsApiController',
         'filePath' => 'applications/dashboard/controllers/api/UserMentionsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'authenticatortypesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AuthenticatorTypesApiController',
         'filePath' => 'applications/dashboard/controllers/api/AuthenticatorTypesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'rolesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RolesApiController',
         'filePath' => 'applications/dashboard/controllers/api/RolesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'dbaapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DbaApiController',
         'filePath' => 'applications/dashboard/controllers/api/DbaApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\resourcesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\API\\ResourcesApiController',
         'filePath' => 'applications/dashboard/controllers/api/ResourcesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\profilefieldsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\ProfileFieldsApiController',
         'filePath' => 'applications/dashboard/controllers/api/ProfileFieldsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\callsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\CallsApiController',
         'filePath' => 'applications/dashboard/controllers/api/CallsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\api\\notificationsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Api\\NotificationsApiController',
         'filePath' => 'applications/dashboard/controllers/api/NotificationsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'usersapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UsersApiController',
         'filePath' => 'applications/dashboard/controllers/api/UsersApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\rolerequestsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RoleRequestsApiController',
         'filePath' => 'applications/dashboard/controllers/api/RoleRequestsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\controllers\\api\\moderationmessagesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Controllers\\Api\\ModerationMessagesApiController',
         'filePath' => 'applications/dashboard/controllers/api/ModerationMessagesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'tokensapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TokensApiController',
         'filePath' => 'applications/dashboard/controllers/api/TokensApiController.php',
         'addonKey' => 'dashboard',
      )),
      'themesapischemes' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ThemesApiSchemes',
         'filePath' => 'applications/dashboard/controllers/api/ThemesApiSchemes.php',
         'addonKey' => 'dashboard',
      )),
      'invitesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'InvitesApiController',
         'filePath' => 'applications/dashboard/controllers/api/InvitesApiController.php',
         'addonKey' => 'dashboard',
      )),
      'addonsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AddonsApiController',
         'filePath' => 'applications/dashboard/controllers/api/AddonsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'applicantsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ApplicantsApiController',
         'filePath' => 'applications/dashboard/controllers/api/ApplicantsApiController.php',
         'addonKey' => 'dashboard',
      )),
      'emailtemplate' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'EmailTemplate',
         'filePath' => 'applications/dashboard/library/class.emailtemplate.php',
         'addonKey' => 'dashboard',
      )),
      'nestedcollectionadapter' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NestedCollectionAdapter',
         'filePath' => 'applications/dashboard/library/class.nestedcollectionadapter.php',
         'addonKey' => 'dashboard',
      )),
      'nestedcollection' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NestedCollection',
         'filePath' => 'applications/dashboard/library/class.nestedcollection.php',
         'addonKey' => 'dashboard',
      )),
      'staticinitializer' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'StaticInitializer',
         'filePath' => 'applications/dashboard/library/class.staticinitializer.php',
         'addonKey' => 'dashboard',
      )),
      'rawemailtemplate' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RawEmailTemplate',
         'filePath' => 'applications/dashboard/library/class.rawemailtemplate.php',
         'addonKey' => 'dashboard',
      )),
      'gdn_iemailtemplate' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Gdn_IEmailTemplate',
         'filePath' => 'applications/dashboard/library/interface.iemailtemplate.php',
         'addonKey' => 'dashboard',
      )),
      'rolecounterprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RoleCounterProvider',
         'filePath' => 'applications/dashboard/library/Menu/RoleCounterProvider.php',
         'addonKey' => 'dashboard',
      )),
      'activitycounterprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActivityCounterProvider',
         'filePath' => 'applications/dashboard/library/Menu/ActivityCounterProvider.php',
         'addonKey' => 'dashboard',
      )),
      'logcounterprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LogCounterProvider',
         'filePath' => 'applications/dashboard/library/Menu/LogCounterProvider.php',
         'addonKey' => 'dashboard',
      )),
      'tiny_diff' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Tiny_diff',
         'filePath' => 'applications/dashboard/models/tiny_diff.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\permissionjunctionmodelinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\PermissionJunctionModelInterface',
         'filePath' => 'applications/dashboard/models/PermissionJunctionModelInterface.php',
         'addonKey' => 'dashboard',
      )),
      'tagmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TagModel',
         'filePath' => 'applications/dashboard/models/class.tagmodel.php',
         'addonKey' => 'dashboard',
      )),
      'sessionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SessionModel',
         'filePath' => 'applications/dashboard/models/class.sessionmodel.php',
         'addonKey' => 'dashboard',
      )),
      'attachmentmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AttachmentModel',
         'filePath' => 'applications/dashboard/models/class.attachmentmodel.php',
         'addonKey' => 'dashboard',
      )),
      'smf2importmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Smf2ImportModel',
         'filePath' => 'applications/dashboard/models/class.smf2importmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\activityemail' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ActivityEmail',
         'filePath' => 'applications/dashboard/models/ActivityEmail.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\moderationmessagestructure' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ModerationMessageStructure',
         'filePath' => 'applications/dashboard/models/ModerationMessageStructure.php',
         'addonKey' => 'dashboard',
      )),
      'vbulletinimportmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'vBulletinImportModel',
         'filePath' => 'applications/dashboard/models/class.vbulletinimportmodel.php',
         'addonKey' => 'dashboard',
      )),
      'localemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LocaleModel',
         'filePath' => 'applications/dashboard/models/class.localemodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\usermentionsinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserMentionsInterface',
         'filePath' => 'applications/dashboard/models/UserMentionsInterface.php',
         'addonKey' => 'dashboard',
      )),
      'permissionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PermissionModel',
         'filePath' => 'applications/dashboard/models/class.permissionmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\legacyprofilefieldmigrator' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\LegacyProfileFieldMigrator',
         'filePath' => 'applications/dashboard/models/LegacyProfileFieldMigrator.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\usermentionsmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserMentionsModel',
         'filePath' => 'applications/dashboard/models/UserMentionsModel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\userleaderservice' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\UserLeaderService',
         'filePath' => 'applications/dashboard/models/UserLeaderService.php',
         'addonKey' => 'dashboard',
      )),
      'usermetamodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserMetaModel',
         'filePath' => 'applications/dashboard/models/class.usermetamodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\userfragment' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserFragment',
         'filePath' => 'applications/dashboard/models/UserFragment.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\profilefieldsopenapi' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ProfileFieldsOpenApi',
         'filePath' => 'applications/dashboard/models/ProfileFieldsOpenApi.php',
         'addonKey' => 'dashboard',
      )),
      'extendeduserfieldsexpander' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ExtendedUserFieldsExpander',
         'filePath' => 'applications/dashboard/models/ExtendedUserFieldsExpander.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\authenticatortypeservice' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\AuthenticatorTypeService',
         'filePath' => 'applications/dashboard/models/AuthenticatorTypeService.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\rolerequestmetamodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RoleRequestMetaModel',
         'filePath' => 'applications/dashboard/models/RoleRequestMetaModel.php',
         'addonKey' => 'dashboard',
      )),
      'importmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ImportModel',
         'filePath' => 'applications/dashboard/models/class.importmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\usersitetotalprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserSiteTotalProvider',
         'filePath' => 'applications/dashboard/models/UserSiteTotalProvider.php',
         'addonKey' => 'dashboard',
      )),
      'invitationmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'InvitationModel',
         'filePath' => 'applications/dashboard/models/class.invitationmodel.php',
         'addonKey' => 'dashboard',
      )),
      'regardingmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RegardingModel',
         'filePath' => 'applications/dashboard/models/class.regardingmodel.php',
         'addonKey' => 'dashboard',
      )),
      'spammodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SpamModel',
         'filePath' => 'applications/dashboard/models/class.spammodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\recordstatusmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RecordStatusModel',
         'filePath' => 'applications/dashboard/models/RecordStatusModel.php',
         'addonKey' => 'dashboard',
      )),
      'userauthenticationnoncemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserAuthenticationNonceModel',
         'filePath' => 'applications/dashboard/models/class.userauthenticationnoncemodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla1importmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla1ImportModel',
         'filePath' => 'applications/dashboard/models/class.vanilla1importmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\recordstatuslogmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RecordStatusLogModel',
         'filePath' => 'applications/dashboard/models/RecordStatusLogModel.php',
         'addonKey' => 'dashboard',
      )),
      'messagemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MessageModel',
         'filePath' => 'applications/dashboard/models/class.messagemodel.php',
         'addonKey' => 'dashboard',
      )),
      'exportmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ExportModel',
         'filePath' => 'applications/dashboard/models/class.exportmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\reflectionaction' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ReflectionAction',
         'filePath' => 'applications/dashboard/models/ReflectionAction.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\aggregatecountoption' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\AggregateCountOption',
         'filePath' => 'applications/dashboard/models/AggregateCountOption.php',
         'addonKey' => 'dashboard',
      )),
      'rolemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RoleModel',
         'filePath' => 'applications/dashboard/models/class.rolemodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\shortcircuitexception' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ShortCircuitException',
         'filePath' => 'applications/dashboard/models/ShortCircuitException.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\remoteresourcemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RemoteResourceModel',
         'filePath' => 'applications/dashboard/models/RemoteResourceModel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\moderationmessagesfilteropenapi' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ModerationMessagesFilterOpenApi',
         'filePath' => 'applications/dashboard/models/ModerationMessagesFilterOpenApi.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\swaggermodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\SwaggerModel',
         'filePath' => 'applications/dashboard/models/SwaggerModel.php',
         'addonKey' => 'dashboard',
      )),
      'logmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LogModel',
         'filePath' => 'applications/dashboard/models/class.logmodel.php',
         'addonKey' => 'dashboard',
      )),
      'updatemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UpdateModel',
         'filePath' => 'applications/dashboard/models/class.updatemodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\profilefieldmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\ProfileFieldModel',
         'filePath' => 'applications/dashboard/models/ProfileFieldModel.php',
         'addonKey' => 'dashboard',
      )),
      'usermodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserModel',
         'filePath' => 'applications/dashboard/models/class.usermodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\userpointsmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\UserPointsModel',
         'filePath' => 'applications/dashboard/models/UserPointsModel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\userleaderquery' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserLeaderQuery',
         'filePath' => 'applications/dashboard/models/UserLeaderQuery.php',
         'addonKey' => 'dashboard',
      )),
      'userauthenticationtokenmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserAuthenticationTokenModel',
         'filePath' => 'applications/dashboard/models/class.userauthenticationtokenmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\aggregatecountableinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\AggregateCountableInterface',
         'filePath' => 'applications/dashboard/models/AggregateCountableInterface.php',
         'addonKey' => 'dashboard',
      )),
      'activitymodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActivityModel',
         'filePath' => 'applications/dashboard/models/class.activitymodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\ssousersexpander' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\SsoUsersExpander',
         'filePath' => 'applications/dashboard/models/SsoUsersExpander.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\usersexpander' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UsersExpander',
         'filePath' => 'applications/dashboard/models/UsersExpander.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\aggregatecountmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\AggregateCountModel',
         'filePath' => 'applications/dashboard/models/AggregateCountModel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\userleaderproviderinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserLeaderProviderInterface',
         'filePath' => 'applications/dashboard/models/UserLeaderProviderInterface.php',
         'addonKey' => 'dashboard',
      )),
      'dbamodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DBAModel',
         'filePath' => 'applications/dashboard/models/class.dbamodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\rolerequestmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RoleRequestModel',
         'filePath' => 'applications/dashboard/models/RoleRequestModel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\recordstatusstructureevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RecordStatusStructureEvent',
         'filePath' => 'applications/dashboard/models/RecordStatusStructureEvent.php',
         'addonKey' => 'dashboard',
      )),
      'userprofilefieldsexpander' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserProfileFieldsExpander',
         'filePath' => 'applications/dashboard/models/UserProfileFieldsExpander.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\rolerequestvalidation' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RoleRequestValidation',
         'filePath' => 'applications/dashboard/models/RoleRequestValidation.php',
         'addonKey' => 'dashboard',
      )),
      'mediamodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MediaModel',
         'filePath' => 'applications/dashboard/models/class.mediamodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\uservisitupdater' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\UserVisitUpdater',
         'filePath' => 'applications/dashboard/models/UserVisitUpdater.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\recordstatusactiveevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\RecordStatusActiveEvent',
         'filePath' => 'applications/dashboard/models/RecordStatusActiveEvent.php',
         'addonKey' => 'dashboard',
      )),
      'banmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BanModel',
         'filePath' => 'applications/dashboard/models/class.banmodel.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\models\\bannerimagemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Models\\BannerImageModel',
         'filePath' => 'applications/dashboard/models/BannerImageModel.php',
         'addonKey' => 'dashboard',
      )),
      'pagermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PagerModule',
         'filePath' => 'applications/dashboard/modules/class.pagermodule.php',
         'addonKey' => 'dashboard',
      )),
      'signedinmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SignedInModule',
         'filePath' => 'applications/dashboard/modules/class.signedinmodule.php',
         'addonKey' => 'dashboard',
      )),
      'cropimagemodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CropImageModule',
         'filePath' => 'applications/dashboard/modules/class.cropimagemodule.php',
         'addonKey' => 'dashboard',
      )),
      'configurationmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConfigurationModule',
         'filePath' => 'applications/dashboard/modules/class.configurationmodule.php',
         'addonKey' => 'dashboard',
      )),
      'recentactivitymodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RecentActivityModule',
         'filePath' => 'applications/dashboard/modules/class.recentactivitymodule.php',
         'addonKey' => 'dashboard',
      )),
      'activityfiltermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActivityFilterModule',
         'filePath' => 'applications/dashboard/modules/class.activityfiltermodule.php',
         'addonKey' => 'dashboard',
      )),
      'tablesummarymodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TableSummaryModule',
         'filePath' => 'applications/dashboard/modules/class.tablesummarymodule.php',
         'addonKey' => 'dashboard',
      )),
      'profilefiltermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ProfileFilterModule',
         'filePath' => 'applications/dashboard/modules/class.profilefiltermodule.php',
         'addonKey' => 'dashboard',
      )),
      'memodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MeModule',
         'filePath' => 'applications/dashboard/modules/class.memodule.php',
         'addonKey' => 'dashboard',
      )),
      'userbanmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserBanModule',
         'filePath' => 'applications/dashboard/modules/class.userbanmodule.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\modules\\communityleadersmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Modules\\CommunityLeadersModule',
         'filePath' => 'applications/dashboard/modules/CommunityLeadersModule.php',
         'addonKey' => 'dashboard',
      )),
      'navmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NavModule',
         'filePath' => 'applications/dashboard/modules/class.navmodule.php',
         'addonKey' => 'dashboard',
      )),
      'togglemenumodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ToggleMenuModule',
         'filePath' => 'applications/dashboard/modules/class.togglemenumodule.php',
         'addonKey' => 'dashboard',
      )),
      'mediaitemmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MediaItemModule',
         'filePath' => 'applications/dashboard/modules/class.mediaitemmodule.php',
         'addonKey' => 'dashboard',
      )),
      'headmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'HeadModule',
         'filePath' => 'applications/dashboard/modules/class.headmodule.php',
         'addonKey' => 'dashboard',
      )),
      'menumodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MenuModule',
         'filePath' => 'applications/dashboard/modules/class.menumodule.php',
         'addonKey' => 'dashboard',
      )),
      'conditionmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConditionModule',
         'filePath' => 'applications/dashboard/modules/class.conditionmodule.php',
         'addonKey' => 'dashboard',
      )),
      'roleapplicationlinksmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RoleApplicationLinksModule',
         'filePath' => 'applications/dashboard/modules/RoleApplicationLinksModule.php',
         'addonKey' => 'dashboard',
      )),
      'tracemodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TraceModule',
         'filePath' => 'applications/dashboard/modules/class.tracemodule.php',
         'addonKey' => 'dashboard',
      )),
      'sitenavmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SiteNavModule',
         'filePath' => 'applications/dashboard/modules/class.sitenavmodule.php',
         'addonKey' => 'dashboard',
      )),
      'userinfomodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserInfoModule',
         'filePath' => 'applications/dashboard/modules/class.userinfomodule.php',
         'addonKey' => 'dashboard',
      )),
      'sidemenumodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SideMenuModule',
         'filePath' => 'applications/dashboard/modules/class.sidemenumodule.php',
         'addonKey' => 'dashboard',
      )),
      'userphotomodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserPhotoModule',
         'filePath' => 'applications/dashboard/modules/class.userphotomodule.php',
         'addonKey' => 'dashboard',
      )),
      'sitetotalsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SiteTotalsModule',
         'filePath' => 'applications/dashboard/modules/class.sitetotalsmodule.php',
         'addonKey' => 'dashboard',
      )),
      'dropdownmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DropdownModule',
         'filePath' => 'applications/dashboard/modules/class.dropdownmodule.php',
         'addonKey' => 'dashboard',
      )),
      'morepagermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MorePagerModule',
         'filePath' => 'applications/dashboard/modules/class.morepagermodule.php',
         'addonKey' => 'dashboard',
      )),
      'guestmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'GuestModule',
         'filePath' => 'applications/dashboard/modules/class.guestmodule.php',
         'addonKey' => 'dashboard',
      )),
      'recentusermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RecentUserModule',
         'filePath' => 'applications/dashboard/modules/class.recentusermodule.php',
         'addonKey' => 'dashboard',
      )),
      'profileoptionsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ProfileOptionsModule',
         'filePath' => 'applications/dashboard/modules/class.profileoptionsmodule.php',
         'addonKey' => 'dashboard',
      )),
      'messagemodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MessageModule',
         'filePath' => 'applications/dashboard/modules/class.messagemodule.php',
         'addonKey' => 'dashboard',
      )),
      'userboxmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserBoxModule',
         'filePath' => 'applications/dashboard/modules/class.userboxmodule.php',
         'addonKey' => 'dashboard',
      )),
      'dashboardnavmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DashboardNavModule',
         'filePath' => 'applications/dashboard/modules/class.dashboardnavmodule.php',
         'addonKey' => 'dashboard',
      )),
      'settingsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SettingsModule',
         'filePath' => 'applications/dashboard/modules/class.settingsmodule.php',
         'addonKey' => 'dashboard',
      )),
      'dashboardhooks' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DashboardHooks',
         'filePath' => 'applications/dashboard/settings/class.hooks.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\addon\\usermentionseventhandler' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Addon\\UserMentionsEventHandler',
         'filePath' => 'applications/dashboard/Addon/UserMentionsEventHandler.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\addon\\dashboardcontainerrules' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Addon\\DashboardContainerRules',
         'filePath' => 'applications/dashboard/Addon/DashboardContainerRules.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\addon\\recordstatuseventhandlers' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Addon\\RecordStatusEventHandlers',
         'filePath' => 'applications/dashboard/Addon/RecordStatusEventHandlers.php',
         'addonKey' => 'dashboard',
      )),
      'searchmemberseventprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SearchMembersEventProvider',
         'filePath' => 'applications/dashboard/Analytics/SearchMembersEventProvider.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\logpostevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\LogPostEvent',
         'filePath' => 'applications/dashboard/Events/LogPostEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\afteruseranonymizeevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\AfterUserAnonymizeEvent',
         'filePath' => 'applications/dashboard/Events/AfterUserAnonymizeEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\userdisciplineevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\UserDisciplineEvent',
         'filePath' => 'applications/dashboard/Events/UserDisciplineEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\notificationevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\NotificationEvent',
         'filePath' => 'applications/dashboard/Events/NotificationEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\searchmembersevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\SearchMembersEvent',
         'filePath' => 'applications/dashboard/Events/SearchMembersEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\userevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\UserEvent',
         'filePath' => 'applications/dashboard/Events/UserEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\events\\userpointevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Events\\UserPointEvent',
         'filePath' => 'applications/dashboard/Events/UserPointEvent.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\layout\\view\\legacyregistrationlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Layout\\View\\LegacyRegistrationLayoutView',
         'filePath' => 'applications/dashboard/Layout/View/LegacyRegistrationLayoutView.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\layout\\view\\legacyprofilelayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Layout\\View\\LegacyProfileLayoutView',
         'filePath' => 'applications/dashboard/Layout/View/LegacyProfileLayoutView.php',
         'addonKey' => 'dashboard',
      )),
      'vanilla\\dashboard\\layout\\view\\legacysigninlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Dashboard\\Layout\\View\\LegacySigninLayoutView',
         'filePath' => 'applications/dashboard/Layout/View/LegacySigninLayoutView.php',
         'addonKey' => 'dashboard',
      )),
      'basetrackablesearchschema' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BaseTrackableSearchSchema',
         'filePath' => 'applications/dashboard/Schemas/BaseTrackableSearchSchema.php',
         'addonKey' => 'dashboard',
      )),
    ),
     'subdir' => '/applications/dashboard',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'DashboardHooks',
      'structure' => '/settings/structure.php',
      'config' => '/settings/configuration.php',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
        0 => 'Vanilla\\Dashboard\\Addon\\DashboardContainerRules',
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
        0 => 'Vanilla\\Dashboard\\Addon\\UserMentionsEventHandler',
        1 => 'Vanilla\\Dashboard\\Addon\\RecordStatusEventHandlers',
      ),
    )),
  )),
  'vanilla' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'allowDisable' => false,
      'description' => 'Vanilla is the sweetest discussion forum on the web.',
      'setupController' => 'setup',
      'url' => 'https://open.vanillaforums.com',
      'license' => 'GPL v2',
      'hidden' => true,
      'icon' => 'vanilla.png',
      'key' => 'vanilla',
      'type' => 'addon',
      'priority' => 10,
      'name' => 'Vanilla',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Vanilla Staff',
          'email' => 'support@vanillaforums.com',
          'homepage' => 'https://open.vanillaforums.com',
        ),
      ),
      'version' => '3.0',
      'oldType' => 'application',
      'keyRaw' => 'Vanilla',
      'Issues' => 
      array (
      ),
    ),
     'classes' => 
    array (
      'moderationcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ModerationController',
         'filePath' => 'applications/vanilla/controllers/class.moderationcontroller.php',
         'addonKey' => 'vanilla',
      )),
      'discussioncontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionController',
         'filePath' => 'applications/vanilla/controllers/class.discussioncontroller.php',
         'addonKey' => 'vanilla',
      )),
      'categoriescontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoriesController',
         'filePath' => 'applications/vanilla/controllers/class.categoriescontroller.php',
         'addonKey' => 'vanilla',
      )),
      'draftscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DraftsController',
         'filePath' => 'applications/vanilla/controllers/class.draftscontroller.php',
         'addonKey' => 'vanilla',
      )),
      'tagscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TagsController',
         'filePath' => 'applications/vanilla/controllers/class.tagscontroller.php',
         'addonKey' => 'vanilla',
      )),
      'postcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PostController',
         'filePath' => 'applications/vanilla/controllers/class.postcontroller.php',
         'addonKey' => 'vanilla',
      )),
      'vanillacontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'VanillaController',
         'filePath' => 'applications/vanilla/controllers/class.vanillacontroller.php',
         'addonKey' => 'vanilla',
      )),
      'discussionscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionsController',
         'filePath' => 'applications/vanilla/controllers/class.discussionscontroller.php',
         'addonKey' => 'vanilla',
      )),
      'categorycontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoryController',
         'filePath' => 'applications/vanilla/controllers/class.categorycontroller.php',
         'addonKey' => 'vanilla',
      )),
      'vanillasettingscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'VanillaSettingsController',
         'filePath' => 'applications/vanilla/controllers/class.vanillasettingscontroller.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\controllers\\pages\\discussionlistpagecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Controllers\\Pages\\DiscussionListPageController',
         'filePath' => 'applications/vanilla/controllers/Pages/DiscussionListPageController.php',
         'addonKey' => 'vanilla',
      )),
      'communitysearchschematrait' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CommunitySearchSchemaTrait',
         'filePath' => 'applications/vanilla/controllers/api/CommunitySearchSchemaTrait.php',
         'addonKey' => 'vanilla',
      )),
      'schedulerapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SchedulerApiController',
         'filePath' => 'applications/vanilla/controllers/api/SchedulerApiController.php',
         'addonKey' => 'vanilla',
      )),
      'sessionsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SessionsApiController',
         'filePath' => 'applications/vanilla/controllers/api/SessionsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\controllers\\api\\discussionsapiindexschema' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Controllers\\Api\\DiscussionsApiIndexSchema',
         'filePath' => 'applications/vanilla/controllers/api/DiscussionsApiIndexSchema.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\controllers\\api\\collectionsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Controllers\\Api\\CollectionsApiController',
         'filePath' => 'applications/vanilla/controllers/api/CollectionsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'commentsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CommentsApiController',
         'filePath' => 'applications/vanilla/controllers/api/CommentsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'categoriesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoriesApiController',
         'filePath' => 'applications/vanilla/controllers/api/CategoriesApiController.php',
         'addonKey' => 'vanilla',
      )),
      'discussionsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionsApiController',
         'filePath' => 'applications/vanilla/controllers/api/DiscussionsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'tagsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TagsApiController',
         'filePath' => 'applications/vanilla/controllers/api/TagsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'discussionexpandschema' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionExpandSchema',
         'filePath' => 'applications/vanilla/controllers/api/DiscussionExpandSchema.php',
         'addonKey' => 'vanilla',
      )),
      'draftsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DraftsApiController',
         'filePath' => 'applications/vanilla/controllers/api/DraftsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\controllers\\api\\widgetsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Controllers\\Api\\WidgetsApiController',
         'filePath' => 'applications/vanilla/controllers/api/WidgetsApiController.php',
         'addonKey' => 'vanilla',
      )),
      'categorycollection' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoryCollection',
         'filePath' => 'applications/vanilla/library/class.categorycollection.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\navigation\\forumcategoryrecordtype' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Navigation\\ForumCategoryRecordType',
         'filePath' => 'applications/vanilla/library/Navigation/ForumCategoryRecordType.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\navigation\\forumbreadcrumbprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Navigation\\ForumBreadcrumbProvider',
         'filePath' => 'applications/vanilla/library/Navigation/ForumBreadcrumbProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\menu\\usercounterprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Menu\\UserCounterProvider',
         'filePath' => 'applications/vanilla/library/Menu/UserCounterProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\discussioncollectionprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\DiscussionCollectionProvider',
         'filePath' => 'applications/vanilla/models/DiscussionCollectionProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\models\\communitynotificationgenerator' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Models\\CommunityNotificationGenerator',
         'filePath' => 'applications/vanilla/models/CommunityNotificationGenerator.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\categorycollectionprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\CategoryCollectionProvider',
         'filePath' => 'applications/vanilla/models/CategoryCollectionProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanillamodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'VanillaModel',
         'filePath' => 'applications/vanilla/models/class.vanillamodel.php',
         'addonKey' => 'vanilla',
      )),
      'categorymodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoryModel',
         'filePath' => 'applications/vanilla/models/class.categorymodel.php',
         'addonKey' => 'vanilla',
      )),
      'commentmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CommentModel',
         'filePath' => 'applications/vanilla/models/class.commentmodel.php',
         'addonKey' => 'vanilla',
      )),
      'draftmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DraftModel',
         'filePath' => 'applications/vanilla/models/class.draftmodel.php',
         'addonKey' => 'vanilla',
      )),
      'discussionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionModel',
         'filePath' => 'applications/vanilla/models/class.discussionmodel.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\discussionmergemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\DiscussionMergeModel',
         'filePath' => 'applications/vanilla/models/DiscussionMergeModel.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\models\\collectionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Models\\CollectionModel',
         'filePath' => 'applications/vanilla/models/CollectionModel.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\models\\collectionrecordproviderinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Models\\CollectionRecordProviderInterface',
         'filePath' => 'applications/vanilla/models/CollectionRecordProviderInterface.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\discussiontypehandler' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\DiscussionTypeHandler',
         'filePath' => 'applications/vanilla/models/DiscussionTypeHandler.php',
         'addonKey' => 'vanilla',
      )),
      'discussionstatusmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionStatusModel',
         'filePath' => 'applications/vanilla/models/DiscussionStatusModel.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\forumquicklinksprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\ForumQuickLinksProvider',
         'filePath' => 'applications/vanilla/models/ForumQuickLinksProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\models\\discussionjsonld' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Models\\DiscussionJsonLD',
         'filePath' => 'applications/vanilla/models/DiscussionJsonLD.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\models\\dirtyrecordmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Models\\DirtyRecordModel',
         'filePath' => 'applications/vanilla/models/DirtyRecordModel.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\models\\authenticatortypeinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Models\\AuthenticatorTypeInterface',
         'filePath' => 'applications/vanilla/models/AuthenticatorTypeInterface.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\totals\\commentsitetotalprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\Totals\\CommentSiteTotalProvider',
         'filePath' => 'applications/vanilla/models/Totals/CommentSiteTotalProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\totals\\categorysitetotalprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\Totals\\CategorySiteTotalProvider',
         'filePath' => 'applications/vanilla/models/Totals/CategorySiteTotalProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\totals\\postsitetotalprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\Totals\\PostSiteTotalProvider',
         'filePath' => 'applications/vanilla/models/Totals/PostSiteTotalProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\models\\totals\\discussionsitetotalprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Models\\Totals\\DiscussionSiteTotalProvider',
         'filePath' => 'applications/vanilla/models/Totals/DiscussionSiteTotalProvider.php',
         'addonKey' => 'vanilla',
      )),
      'followedcategoriesmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'FollowedCategoriesModule',
         'filePath' => 'applications/vanilla/modules/class.followedcategoriesmodule.php',
         'addonKey' => 'vanilla',
      )),
      'usercommentsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserCommentsModule',
         'filePath' => 'applications/vanilla/modules/class.usercommentsmodule.php',
         'addonKey' => 'vanilla',
      )),
      'discussionsortermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionSorterModule',
         'filePath' => 'applications/vanilla/modules/class.discussionsortermodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\foundationcategoriesgridmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\FoundationCategoriesGridModule',
         'filePath' => 'applications/vanilla/modules/FoundationCategoriesGridModule.php',
         'addonKey' => 'vanilla',
      )),
      'bookmarkedmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BookmarkedModule',
         'filePath' => 'applications/vanilla/modules/class.bookmarkedmodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\announcementwidgetmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\AnnouncementWidgetModule',
         'filePath' => 'applications/vanilla/modules/AnnouncementWidgetModule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\rssmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\RSSModule',
         'filePath' => 'applications/vanilla/modules/RSSModule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forums\\modules\\discussiontabfactory' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forums\\Modules\\DiscussionTabFactory',
         'filePath' => 'applications/vanilla/modules/DiscussionTabFactory.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\categoriesmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\CategoriesModule',
         'filePath' => 'applications/vanilla/modules/CategoriesModule.php',
         'addonKey' => 'vanilla',
      )),
      'tagmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'TagModule',
         'filePath' => 'applications/vanilla/modules/class.tagmodule.php',
         'addonKey' => 'vanilla',
      )),
      'draftsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DraftsModule',
         'filePath' => 'applications/vanilla/modules/class.draftsmodule.php',
         'addonKey' => 'vanilla',
      )),
      'promotedcontentmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PromotedContentModule',
         'filePath' => 'applications/vanilla/modules/class.promotedcontentmodule.php',
         'addonKey' => 'vanilla',
      )),
      'discussionsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionsModule',
         'filePath' => 'applications/vanilla/modules/class.discussionsmodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\basediscussionwidgetmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\BaseDiscussionWidgetModule',
         'filePath' => 'applications/vanilla/modules/BaseDiscussionWidgetModule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\discussionwidgetmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\DiscussionWidgetModule',
         'filePath' => 'applications/vanilla/modules/DiscussionWidgetModule.php',
         'addonKey' => 'vanilla',
      )),
      'categoriesmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoriesModule',
         'filePath' => 'applications/vanilla/modules/class.categoriesmodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\foundationshimoptions' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\FoundationShimOptions',
         'filePath' => 'applications/vanilla/modules/FoundationShimOptions.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\calltoactionmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\CallToActionModule',
         'filePath' => 'applications/vanilla/modules/CallToActionModule.php',
         'addonKey' => 'vanilla',
      )),
      'userdiscussionsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserDiscussionsModule',
         'filePath' => 'applications/vanilla/modules/class.userdiscussionsmodule.php',
         'addonKey' => 'vanilla',
      )),
      'categoryfollowtogglemodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoryFollowToggleModule',
         'filePath' => 'applications/vanilla/modules/class.categoryfollowtogglemodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\userspotlightmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\UserSpotlightModule',
         'filePath' => 'applications/vanilla/modules/UserSpotlightModule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\searchwidgetmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\SearchWidgetModule',
         'filePath' => 'applications/vanilla/modules/SearchWidgetModule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\discussionlistmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\DiscussionListModule',
         'filePath' => 'applications/vanilla/modules/DiscussionListModule.php',
         'addonKey' => 'vanilla',
      )),
      'categorymoderatorsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CategoryModeratorsModule',
         'filePath' => 'applications/vanilla/modules/class.categorymoderatorsmodule.php',
         'addonKey' => 'vanilla',
      )),
      'discussionfiltermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionFilterModule',
         'filePath' => 'applications/vanilla/modules/class.discussionfiltermodule.php',
         'addonKey' => 'vanilla',
      )),
      'flatcategorymodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'FlatCategoryModule',
         'filePath' => 'applications/vanilla/modules/class.flatcategorymodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\foundationdiscussionsshim' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\FoundationDiscussionsShim',
         'filePath' => 'applications/vanilla/modules/FoundationDiscussionsShim.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\modules\\foundationcategoriesshim' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Modules\\FoundationCategoriesShim',
         'filePath' => 'applications/vanilla/modules/FoundationCategoriesShim.php',
         'addonKey' => 'vanilla',
      )),
      'discussionssortfiltermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionsSortFilterModule',
         'filePath' => 'applications/vanilla/modules/class.discussionssortfiltermodule.php',
         'addonKey' => 'vanilla',
      )),
      'newdiscussionmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NewDiscussionModule',
         'filePath' => 'applications/vanilla/modules/class.newdiscussionmodule.php',
         'addonKey' => 'vanilla',
      )),
      'vanillahooks' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'VanillaHooks',
         'filePath' => 'applications/vanilla/settings/class.hooks.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\addon\\systemtokenrotationjobs' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Addon\\SystemTokenRotationJobs',
         'filePath' => 'applications/vanilla/Addon/SystemTokenRotationJobs.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\addon\\forumcontainerrules' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Addon\\ForumContainerRules',
         'filePath' => 'applications/vanilla/Addon/ForumContainerRules.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\addon\\pagevieweventhandler' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Addon\\PageViewEventHandler',
         'filePath' => 'applications/vanilla/Addon/PageViewEventHandler.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\embeddedcontent\\factories\\discussionembedfactory' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\EmbeddedContent\\Factories\\DiscussionEmbedFactory',
         'filePath' => 'applications/vanilla/EmbeddedContent/Factories/DiscussionEmbedFactory.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\embeddedcontent\\factories\\commentembedfactory' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\EmbeddedContent\\Factories\\CommentEmbedFactory',
         'filePath' => 'applications/vanilla/EmbeddedContent/Factories/CommentEmbedFactory.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\discussionstatusevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\DiscussionStatusEvent',
         'filePath' => 'applications/vanilla/Events/DiscussionStatusEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\pageviewevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\PageViewEvent',
         'filePath' => 'applications/vanilla/Events/PageViewEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\dirtyrecordevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\DirtyRecordEvent',
         'filePath' => 'applications/vanilla/Events/DirtyRecordEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\commentevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\CommentEvent',
         'filePath' => 'applications/vanilla/Events/CommentEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\runnerinterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\RunnerInterface',
         'filePath' => 'applications/vanilla/Events/RunnerInterface.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\searchdiscussionevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\SearchDiscussionEvent',
         'filePath' => 'applications/vanilla/Events/SearchDiscussionEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\categoryevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\CategoryEvent',
         'filePath' => 'applications/vanilla/Events/CategoryEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\discussiontagevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\DiscussionTagEvent',
         'filePath' => 'applications/vanilla/Events/DiscussionTagEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\events\\discussionevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Events\\DiscussionEvent',
         'filePath' => 'applications/vanilla/Events/DiscussionEvent.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\layout\\categoryrecordprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Layout\\CategoryRecordProvider',
         'filePath' => 'applications/vanilla/Layout/CategoryRecordProvider.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\layout\\view\\legacycategorylistlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Layout\\View\\LegacyCategoryListLayoutView',
         'filePath' => 'applications/vanilla/Layout/View/LegacyCategoryListLayoutView.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\layout\\view\\discussionlistlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Layout\\View\\DiscussionListLayoutView',
         'filePath' => 'applications/vanilla/Layout/View/DiscussionListLayoutView.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\layout\\view\\legacydiscussionthreadlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Layout\\View\\LegacyDiscussionThreadLayoutView',
         'filePath' => 'applications/vanilla/Layout/View/LegacyDiscussionThreadLayoutView.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\layout\\view\\legacynewdiscussionlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Layout\\View\\LegacyNewDiscussionLayoutView',
         'filePath' => 'applications/vanilla/Layout/View/LegacyNewDiscussionLayoutView.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\layout\\middleware\\categoryfiltermiddleware' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Layout\\Middleware\\CategoryFilterMiddleware',
         'filePath' => 'applications/vanilla/Layout/Middleware/CategoryFilterMiddleware.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\schemas\\categoryfragmentschema' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Schemas\\CategoryFragmentSchema',
         'filePath' => 'applications/vanilla/Schemas/CategoryFragmentSchema.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\schemas\\postfragmentschema' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Schemas\\PostFragmentSchema',
         'filePath' => 'applications/vanilla/Schemas/PostFragmentSchema.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\community\\schemas\\abstracttabsearchformschema' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Community\\Schemas\\AbstractTabSearchFormSchema',
         'filePath' => 'applications/vanilla/Schemas/AbstractTabSearchFormSchema.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\categorysearchexclusioninterface' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\CategorySearchExclusionInterface',
         'filePath' => 'applications/vanilla/Search/CategorySearchExclusionInterface.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\discussionsearchtype' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\DiscussionSearchType',
         'filePath' => 'applications/vanilla/Search/DiscussionSearchType.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\categorysearchtype' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\CategorySearchType',
         'filePath' => 'applications/vanilla/Search/CategorySearchType.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\usersearchtype' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\UserSearchType',
         'filePath' => 'applications/vanilla/Search/UserSearchType.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\commentsearchtype' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\CommentSearchType',
         'filePath' => 'applications/vanilla/Search/CommentSearchType.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\discussionsearchresultitem' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\DiscussionSearchResultItem',
         'filePath' => 'applications/vanilla/Search/DiscussionSearchResultItem.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\usersearchresultitem' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\UserSearchResultItem',
         'filePath' => 'applications/vanilla/Search/UserSearchResultItem.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\search\\commentsearchresultitem' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Search\\CommentSearchResultItem',
         'filePath' => 'applications/vanilla/Search/CommentSearchResultItem.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\discussionswidgetschematrait' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\DiscussionsWidgetSchemaTrait',
         'filePath' => 'applications/vanilla/Widgets/DiscussionsWidgetSchemaTrait.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\discussiondiscussionswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\DiscussionDiscussionsWidget',
         'filePath' => 'applications/vanilla/Widgets/DiscussionDiscussionsWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\featuredcollectionswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\FeaturedCollectionsWidget',
         'filePath' => 'applications/vanilla/Widgets/FeaturedCollectionsWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\rsswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\RSSWidget',
         'filePath' => 'applications/vanilla/Widgets/RSSWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\guestcalltoactionwidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\GuestCallToActionWidget',
         'filePath' => 'applications/vanilla/Widgets/GuestCallToActionWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\rsswidgettrait' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\RssWidgetTrait',
         'filePath' => 'applications/vanilla/Widgets/RssWidgetTrait.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\categorieswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\CategoriesWidget',
         'filePath' => 'applications/vanilla/Widgets/CategoriesWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\userspotlightwidgettrait' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\UserSpotlightWidgetTrait',
         'filePath' => 'applications/vanilla/Widgets/UserSpotlightWidgetTrait.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\newpostwidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\NewPostWidget',
         'filePath' => 'applications/vanilla/Widgets/NewPostWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\sitetotalswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\SiteTotalsWidget',
         'filePath' => 'applications/vanilla/Widgets/SiteTotalsWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\userspotlightwidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\UserSpotlightWidget',
         'filePath' => 'applications/vanilla/Widgets/UserSpotlightWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\discussionlistasset' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\DiscussionListAsset',
         'filePath' => 'applications/vanilla/Widgets/DiscussionListAsset.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\tagwidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\TagWidget',
         'filePath' => 'applications/vanilla/Widgets/TagWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\calltoactionwidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\CallToActionWidget',
         'filePath' => 'applications/vanilla/Widgets/CallToActionWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\discussionannouncementswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\DiscussionAnnouncementsWidget',
         'filePath' => 'applications/vanilla/Widgets/DiscussionAnnouncementsWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\tabswidget' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\TabsWidget',
         'filePath' => 'applications/vanilla/Widgets/TabsWidget.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\calltoactionwidgettrait' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\CallToActionWidgetTrait',
         'filePath' => 'applications/vanilla/Widgets/CallToActionWidgetTrait.php',
         'addonKey' => 'vanilla',
      )),
      'vanilla\\forum\\widgets\\categorieswidgettrait' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Forum\\Widgets\\CategoriesWidgetTrait',
         'filePath' => 'applications/vanilla/Widgets/CategoriesWidgetTrait.php',
         'addonKey' => 'vanilla',
      )),
    ),
     'subdir' => '/applications/vanilla',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'VanillaHooks',
      'structure' => '/settings/structure.php',
      'config' => '/settings/configuration.php',
      'bootstrap' => '/bootstrap.php',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
        0 => 'Vanilla\\Forum\\Addon\\ForumContainerRules',
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
        0 => 'Vanilla\\Forum\\Addon\\SystemTokenRotationJobs',
        1 => 'Vanilla\\Forum\\Addon\\PageViewEventHandler',
      ),
    )),
  )),
  'conversations' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'description' => 'An improvement upon existing private messaging tools, Conversations allows multiple users to take part in private conversations.',
      'setupController' => 'setup',
      'url' => 'https://open.vanillaforums.com',
      'license' => 'GPL-2.0-only',
      'icon' => 'conversations.png',
      'key' => 'conversations',
      'type' => 'addon',
      'priority' => 10,
      'name' => 'Conversations',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Vanilla Staff',
          'email' => 'support@vanillaforums.com',
          'homepage' => 'https://open.vanillaforums.com',
        ),
      ),
      'version' => '3.0',
      'oldType' => 'application',
      'keyRaw' => 'Conversations',
      'Issues' => 
      array (
      ),
    ),
     'classes' => 
    array (
      'conversationscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationsController',
         'filePath' => 'applications/conversations/controllers/class.conversationscontroller.php',
         'addonKey' => 'conversations',
      )),
      'messagescontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MessagesController',
         'filePath' => 'applications/conversations/controllers/class.messagescontroller.php',
         'addonKey' => 'conversations',
      )),
      'messagesapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MessagesApiController',
         'filePath' => 'applications/conversations/controllers/api/MessagesApiController.php',
         'addonKey' => 'conversations',
      )),
      'conversationsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationsApiController',
         'filePath' => 'applications/conversations/controllers/api/ConversationsApiController.php',
         'addonKey' => 'conversations',
      )),
      'conversationcounterprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationCounterProvider',
         'filePath' => 'applications/conversations/library/Menu/ConversationCounterProvider.php',
         'addonKey' => 'conversations',
      )),
      'conversationmessagemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationMessageModel',
         'filePath' => 'applications/conversations/models/class.conversationmessagemodel.php',
         'addonKey' => 'conversations',
      )),
      'conversationsmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationsModel',
         'filePath' => 'applications/conversations/models/class.conversationsmodel.php',
         'addonKey' => 'conversations',
      )),
      'conversationmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationModel',
         'filePath' => 'applications/conversations/models/class.conversationmodel.php',
         'addonKey' => 'conversations',
      )),
      'inboxmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'InboxModule',
         'filePath' => 'applications/conversations/modules/class.inboxmodule.php',
         'addonKey' => 'conversations',
      )),
      'addpeoplemodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AddPeopleModule',
         'filePath' => 'applications/conversations/modules/class.addpeoplemodule.php',
         'addonKey' => 'conversations',
      )),
      'inthisconversationmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'InThisConversationModule',
         'filePath' => 'applications/conversations/modules/class.inthisconversationmodule.php',
         'addonKey' => 'conversations',
      )),
      'newconversationmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NewConversationModule',
         'filePath' => 'applications/conversations/modules/class.newconversationmodule.php',
         'addonKey' => 'conversations',
      )),
      'clearhistorymodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ClearHistoryModule',
         'filePath' => 'applications/conversations/modules/class.clearhistorymodule.php',
         'addonKey' => 'conversations',
      )),
      'conversationshooks' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ConversationsHooks',
         'filePath' => 'applications/conversations/settings/class.hooks.php',
         'addonKey' => 'conversations',
      )),
      'vanilla\\conversations\\layout\\legacymessageinboxlayoutview' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Conversations\\Layout\\LegacyMessageInboxLayoutView',
         'filePath' => 'applications/conversations/Layout/View/LegacyMessageInboxLayoutView.php',
         'addonKey' => 'conversations',
      )),
    ),
     'subdir' => '/applications/conversations',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'ConversationsHooks',
      'structure' => '/settings/structure.php',
      'config' => '/settings/configuration.php',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'yaga' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Yet Another Gamification Application',
      'description' => 'Yaga provides customizable reactions, badges, and ranks for your Vanilla forum software. Increase user activity by letting users react to content with emotions. Give users badges based on statistics and engagement in your community. Create and award custom badges for special events and recognition. Award Ranks which can confer different (configurable) permissions based on community perception and participation.',
      'version' => '2.0.2',
      'url' => 'http://github.com/bleistivt/yaga',
      'license' => 'GPLv2',
      'mobileFriendly' => true,
      'hasLocale' => true,
      'settingsUrl' => '/yaga/settings',
      'settingsPermission' => 'Garden.Settings.Manage',
      'key' => 'yaga',
      'type' => 'addon',
      'priority' => 10,
      'registerPermissions' => 
      array (
        'Yaga.Reactions.Add' => 0,
        'Yaga.Reactions.Manage' => 0,
        'Yaga.Reactions.View' => 1,
        'Yaga.Reactions.Edit' => 0,
        'Yaga.Badges.Add' => 0,
        'Yaga.Badges.Manage' => 0,
        'Yaga.Badges.View' => 1,
        'Yaga.Ranks.Add' => 0,
        'Yaga.Ranks.Manage' => 0,
        'Yaga.Ranks.View' => 1,
      ),
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Zachary Doll',
          'email' => 'hgtonight@daklutz.com',
          'homepage' => 'http://www.daklutz.com',
        ),
        1 => 
        array (
          'name' => 'Bleistivt',
          'homepage' => 'bleistivt.net',
        ),
      ),
      'oldType' => 'plugin',
      'Issues' => 
      array (
      ),
    ),
     'classes' => 
    array (
      'yagaplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'YagaPlugin',
         'filePath' => 'plugins/yaga/class.yaga.plugin.php',
         'addonKey' => 'yaga',
      )),
      'actioncontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActionController',
         'filePath' => 'plugins/yaga/controllers/class.actioncontroller.php',
         'addonKey' => 'yaga',
      )),
      'yagacontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'YagaController',
         'filePath' => 'plugins/yaga/controllers/class.yagacontroller.php',
         'addonKey' => 'yaga',
      )),
      'bestcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BestController',
         'filePath' => 'plugins/yaga/controllers/class.bestcontroller.php',
         'addonKey' => 'yaga',
      )),
      'rankcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RankController',
         'filePath' => 'plugins/yaga/controllers/class.rankcontroller.php',
         'addonKey' => 'yaga',
      )),
      'reactcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactController',
         'filePath' => 'plugins/yaga/controllers/class.reactcontroller.php',
         'addonKey' => 'yaga',
      )),
      'badgecontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BadgeController',
         'filePath' => 'plugins/yaga/controllers/class.badgecontroller.php',
         'addonKey' => 'yaga',
      )),
      'yaga' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Yaga',
         'filePath' => 'plugins/yaga/library/class.yaga.php',
         'addonKey' => 'yaga',
      )),
      'yagarule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'YagaRule',
         'filePath' => 'plugins/yaga/library/interface.yagarule.php',
         'addonKey' => 'yaga',
      )),
      'awardcombo' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'AwardCombo',
         'filePath' => 'plugins/yaga/library/rules/class.awardcombo.php',
         'addonKey' => 'yaga',
      )),
      'commentmarathon' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CommentMarathon',
         'filePath' => 'plugins/yaga/library/rules/class.commentmarathon.php',
         'addonKey' => 'yaga',
      )),
      'qnaansercount' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'QnAAnserCount',
         'filePath' => 'plugins/yaga/library/rules/class.qnaanswercount.php',
         'addonKey' => 'yaga',
      )),
      'commentcount' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CommentCount',
         'filePath' => 'plugins/yaga/library/rules/class.commentcount.php',
         'addonKey' => 'yaga',
      )),
      'manualaward' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ManualAward',
         'filePath' => 'plugins/yaga/library/rules/class.manualaward.php',
         'addonKey' => 'yaga',
      )),
      'discussioncount' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionCount',
         'filePath' => 'plugins/yaga/library/rules/class.discussioncount.php',
         'addonKey' => 'yaga',
      )),
      'discussionbodylength' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionBodyLength',
         'filePath' => 'plugins/yaga/library/rules/class.discussionbodylength.php',
         'addonKey' => 'yaga',
      )),
      'reflexcomment' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReflexComment',
         'filePath' => 'plugins/yaga/library/rules/class.reflexcomment.php',
         'addonKey' => 'yaga',
      )),
      'cakedaypost' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'CakeDayPost',
         'filePath' => 'plugins/yaga/library/rules/class.cakedaypost.php',
         'addonKey' => 'yaga',
      )),
      'discussionpagecount' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionPageCount',
         'filePath' => 'plugins/yaga/library/rules/class.discussionpagecount.php',
         'addonKey' => 'yaga',
      )),
      'hasmentioned' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'HasMentioned',
         'filePath' => 'plugins/yaga/library/rules/class.hasmentioned.php',
         'addonKey' => 'yaga',
      )),
      'lengthofservice' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LengthOfService',
         'filePath' => 'plugins/yaga/library/rules/class.lengthofservice.php',
         'addonKey' => 'yaga',
      )),
      'necropost' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NecroPost',
         'filePath' => 'plugins/yaga/library/rules/class.necropost.php',
         'addonKey' => 'yaga',
      )),
      'photoexists' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PhotoExists',
         'filePath' => 'plugins/yaga/library/rules/class.photoexists.php',
         'addonKey' => 'yaga',
      )),
      'unknownrule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UnknownRule',
         'filePath' => 'plugins/yaga/library/rules/class.unknownrule.php',
         'addonKey' => 'yaga',
      )),
      'reactioncount' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionCount',
         'filePath' => 'plugins/yaga/library/rules/class.reactioncount.php',
         'addonKey' => 'yaga',
      )),
      'socialconnection' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'SocialConnection',
         'filePath' => 'plugins/yaga/library/rules/class.socialconnection.php',
         'addonKey' => 'yaga',
      )),
      'postreactions' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PostReactions',
         'filePath' => 'plugins/yaga/library/rules/class.postreactions.php',
         'addonKey' => 'yaga',
      )),
      'postcount' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PostCount',
         'filePath' => 'plugins/yaga/library/rules/class.postcount.php',
         'addonKey' => 'yaga',
      )),
      'newbiecomment' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'NewbieComment',
         'filePath' => 'plugins/yaga/library/rules/class.newbiecomment.php',
         'addonKey' => 'yaga',
      )),
      'holidayvisit' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'HolidayVisit',
         'filePath' => 'plugins/yaga/library/rules/class.holidayvisit.php',
         'addonKey' => 'yaga',
      )),
      'discussioncategory' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionCategory',
         'filePath' => 'plugins/yaga/library/rules/class.discussioncategory.php',
         'addonKey' => 'yaga',
      )),
      'actionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActionModel',
         'filePath' => 'plugins/yaga/models/class.actionmodel.php',
         'addonKey' => 'yaga',
      )),
      'badgemodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BadgeModel',
         'filePath' => 'plugins/yaga/models/class.badgemodel.php',
         'addonKey' => 'yaga',
      )),
      'actedmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ActedModel',
         'filePath' => 'plugins/yaga/models/class.actedmodel.php',
         'addonKey' => 'yaga',
      )),
      'reactionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionModel',
         'filePath' => 'plugins/yaga/models/class.reactionmodel.php',
         'addonKey' => 'yaga',
      )),
      'rankmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RankModel',
         'filePath' => 'plugins/yaga/models/class.rankmodel.php',
         'addonKey' => 'yaga',
      )),
      'badgeawardmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BadgeAwardModel',
         'filePath' => 'plugins/yaga/models/class.badgeawardmodel.php',
         'addonKey' => 'yaga',
      )),
      'badgesmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BadgesModule',
         'filePath' => 'plugins/yaga/modules/class.badgesmodule.php',
         'addonKey' => 'yaga',
      )),
      'leaderboardmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LeaderBoardModule',
         'filePath' => 'plugins/yaga/modules/class.leaderboardmodule.php',
         'addonKey' => 'yaga',
      )),
      'bestfiltermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BestFilterModule',
         'filePath' => 'plugins/yaga/modules/class.bestfiltermodule.php',
         'addonKey' => 'yaga',
      )),
    ),
     'subdir' => '/plugins/yaga',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
      'fr' => 
      array (
        0 => '/locale/fr.php',
      ),
      'de' => 
      array (
        0 => '/locale/de.php',
      ),
      'pt_BR' => 
      array (
        0 => '/locale/pt_BR.php',
      ),
      'fi' => 
      array (
        0 => '/locale/fi.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'YagaPlugin',
      'structure' => '/settings/structure.php',
      'config' => '/settings/configuration.php',
      'bootstrap' => '/settings/bootstrap.php',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'rich-editor' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Rich Editor',
      'description' => 'The new WYSIWYG editor for Vanilla. Easy to use, with support for rich content embedding. Also supports a more minimal text mode, Markdown, and BBCode.',
      'version' => '1.0.1',
      'mobileFriendly' => true,
      'hasLocale' => true,
      'icon' => 'rich_editor.png',
      'key' => 'rich-editor',
      'type' => 'addon',
      'x-priority-note' => 'Make sure this runs before the \'editor\' addon.',
      'priority' => 101,
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Adam Charron',
          'email' => 'adam.c@vanillaforums.com',
          'homepage' => 'http://www.vanillaforums.com',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.5',
        'dashboard' => '>= 2.6',
      ),
      'oldType' => 'plugin',
      'Issues' => 
      array (
      ),
    ),
     'classes' => 
    array (
      'richeditorplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RichEditorPlugin',
         'filePath' => 'plugins/rich-editor/RichEditorPlugin.php',
         'addonKey' => 'rich-editor',
      )),
      'richapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RichApiController',
         'filePath' => 'plugins/rich-editor/controllers/api/RichApiController.php',
         'addonKey' => 'rich-editor',
      )),
      'richeditorsitemetaextra' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'RichEditorSiteMetaExtra',
         'filePath' => 'plugins/rich-editor/models/RichEditorSiteMetaExtra.php',
         'addonKey' => 'rich-editor',
      )),
    ),
     'subdir' => '/plugins/rich-editor',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'RichEditorPlugin',
      'bootstrap' => '/bootstrap.php',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'vanillastats' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Vanilla Statistics',
      'description' => 'Adds helpful graphs and information about activity on your forum over time (new users, discussions, comments, and pageviews).',
      'version' => '2.0.7',
      'mobileFriendly' => true,
      'icon' => 'vanilla_stats.png',
      'key' => 'vanillastats',
      'type' => 'addon',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Vanilla Staff',
          'email' => 'support@vanillaforums.com',
          'homepage' => 'http://www.vanillaforums.com',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.0.18',
      ),
      'oldType' => 'plugin',
      'keyRaw' => 'VanillaStats',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'vanillastatsplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'VanillaStatsPlugin',
         'filePath' => 'plugins/VanillaStats/class.vanillastats.plugin.php',
         'addonKey' => 'vanillastats',
      )),
    ),
     'subdir' => '/plugins/VanillaStats',
     'translations' => 
    array (
    ),
     'special' => 
    array (
      'plugin' => 'VanillaStatsPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'profileextender' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Profile Extender',
      'description' => 'Add fields (like status, location, or gamer tags) to profiles and registration.',
      'version' => '3.0.2',
      'mobileFriendly' => true,
      'settingsUrl' => '/dashboard/settings/profileextender',
      'usePopupSettings' => false,
      'settingsPermission' => 'Garden.Settings.Manage',
      'icon' => 'profile-extender.png',
      'key' => 'profileextender',
      'type' => 'addon',
      'hidden' => true,
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Lincoln Russell',
          'email' => 'lincoln@vanillaforums.com',
          'homepage' => 'http://lincolnwebs.com',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.1',
      ),
      'oldType' => 'plugin',
      'keyRaw' => 'ProfileExtender',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'profileextenderplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ProfileExtenderPlugin',
         'filePath' => 'plugins/ProfileExtender/class.profileextender.plugin.php',
         'addonKey' => 'profileextender',
      )),
      'vanilla\\profileextender\\addon\\profileextendercontainerrules' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\ProfileExtender\\Addon\\ProfileExtenderContainerRules',
         'filePath' => 'plugins/ProfileExtender/Addon/ProfileExtenderContainerRules.php',
         'addonKey' => 'profileextender',
      )),
      'vanilla\\profileextender\\models\\extendedusersexpander' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\ProfileExtender\\Models\\ExtendedUsersExpander',
         'filePath' => 'plugins/ProfileExtender/Models/ExtendedUsersExpander.php',
         'addonKey' => 'profileextender',
      )),
    ),
     'subdir' => '/plugins/ProfileExtender',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'ProfileExtenderPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
        0 => 'Vanilla\\ProfileExtender\\Addon\\ProfileExtenderContainerRules',
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'swagger-ui' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'key' => 'swagger-ui',
      'name' => 'API v2 Docs',
      'description' => 'API v2 documentation customized for your community right in your dashboard.',
      'version' => '2.0',
      'license' => 'GPLv2',
      'type' => 'addon',
      'oldType' => 'plugin',
      'require' => 
      array (
        'Vanilla' => '^2.6',
      ),
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Todd Burry',
          'email' => 'todd@vanillaforums.com',
        ),
      ),
      'icon' => 'swagger.png',
      'hidden' => true,
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'vanilla\\swaggerui\\swaggeruiplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\SwaggerUI\\SwaggerUIPlugin',
         'filePath' => 'plugins/swagger-ui/SwaggerUIPlugin.php',
         'addonKey' => 'swagger-ui',
      )),
    ),
     'subdir' => '/plugins/swagger-ui',
     'translations' => 
    array (
    ),
     'special' => 
    array (
      'plugin' => 'Vanilla\\SwaggerUI\\SwaggerUIPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'discussionpolls' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Discussion Polls',
      'description' => 'A plugin that allows creating polls that attach to a discussion. Respects permissions.',
      'version' => '1.3.6',
      'registerPermissions' => 
      array (
        0 => 'Plugins.DiscussionPolls.Add',
        1 => 'Plugins.DiscussionPolls.View',
        2 => 'Plugins.DiscussionPolls.Vote',
        3 => 'Plugins.DiscussionPolls.Manage',
      ),
      'mobileFriendly' => true,
      'settingsUrl' => '/dashboard/settings/discussionpolls',
      'settingsPermission' => 'Garden.Settings.Manage',
      'license' => 'GPLv3',
      'key' => 'discussionpolls',
      'type' => 'addon',
      'priority' => 100,
      'oldType' => 'plugin',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Zachary Doll',
          'email' => 'hgtonight@daklutz.com',
          'homepage' => 'http://www.daklutz.com',
        ),
      ),
      'Issues' => 
      array (
      ),
      'keyRaw' => 'DiscussionPolls',
    ),
     'classes' => 
    array (
      'discussionpollsplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionPollsPlugin',
         'filePath' => 'plugins/DiscussionPolls/class.discussionpolls.plugin.php',
         'addonKey' => 'DiscussionPolls',
      )),
      'discussionpollsmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'DiscussionPollsModel',
         'filePath' => 'plugins/DiscussionPolls/class.discussionpollsmodel.php',
         'addonKey' => 'DiscussionPolls',
      )),
    ),
     'subdir' => '/plugins/DiscussionPolls',
     'translations' => 
    array (
      'fi' => 
      array (
        0 => '/locale/fi.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'DiscussionPollsPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'vanillicon' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Vanillicon',
      'description' => 'Provides fun default user icons from vanillicon.com.',
      'version' => '2.1.0',
      'mobileFriendly' => true,
      'settingsUrl' => '/settings/vanillicon',
      'settingsPermission' => 'Garden.Settings.Manage',
      'icon' => 'vanillicon.png',
      'key' => 'vanillicon',
      'type' => 'addon',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Todd Burry',
          'email' => 'todd@vanillaforums.com',
          'homepage' => 'https://open.vanillaforums.com/profile/todd',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.0.18',
      ),
      'oldType' => 'plugin',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'vanilliconplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'VanilliconPlugin',
         'filePath' => 'plugins/vanillicon/class.vanillicon.plugin.php',
         'addonKey' => 'vanillicon',
      )),
    ),
     'subdir' => '/plugins/vanillicon',
     'translations' => 
    array (
    ),
     'special' => 
    array (
      'plugin' => 'VanilliconPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'puheetplatformintegration' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Puheet Platform Integration',
      'className' => 'PuheetPlatformIntegrationPlugin',
      'description' => 'Integrates Puheet Platform registration and authentication to the forum app.',
      'version' => '0.1',
      'mobileFriendly' => true,
      'settingsUrl' => '/dashboard/settings/puheetplatformintegration',
      'settingsPermission' => 'Garden.Settings.Manage',
      'icon' => 'puheet.png',
      'key' => 'puheetplatformintegration',
      'type' => 'addon',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Puheet.com',
          'email' => 'support@puheet.com',
          'homepage' => 'http://www.puheet.com',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=3.3',
      ),
      'oldType' => 'plugin',
      'Issues' => 
      array (
      ),
      'priority' => 100,
      'keyRaw' => 'PuheetPlatformIntegration',
    ),
     'classes' => 
    array (
      'puheetplatformintegrationplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PuheetPlatformIntegrationPlugin',
         'filePath' => 'plugins/PuheetPlatformIntegration/PuheetPlatformIntegrationPlugin.php',
         'addonKey' => 'PuheetPlatformIntegration',
      )),
      'puheetcontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PuheetController',
         'filePath' => 'plugins/PuheetPlatformIntegration/controllers/class.puheetcontroller.php',
         'addonKey' => 'PuheetPlatformIntegration',
      )),
    ),
     'subdir' => '/plugins/PuheetPlatformIntegration',
     'translations' => 
    array (
    ),
     'special' => 
    array (
      'plugin' => 'PuheetPlatformIntegrationPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'pockets' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Pockets',
      'description' => 'Administrators may add raw HTML to various places on the site. This plugin is very powerful, but can easily break your site if you make a mistake.',
      'version' => '1.5.1',
      'registerPermissions' => 
      array (
        'Plugins.Pockets.Manage' => 'Garden.Settings.Manage',
        0 => 'Garden.NoAds.Allow',
      ),
      'settingsUrl' => '/settings/pockets',
      'usePopupSettings' => false,
      'settingsPermission' => 'Plugins.Pockets.Manage',
      'mobileFriendly' => true,
      'hasLocale' => true,
      'license' => 'GPL-2.0-only',
      'icon' => 'pocket.png',
      'key' => 'pockets',
      'type' => 'addon',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Todd Burry',
          'email' => 'todd@vanillaforums.com',
          'homepage' => 'http://vanillaforums.org/profile/todd',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.7',
      ),
      'oldType' => 'plugin',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'pocketsplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PocketsPlugin',
         'filePath' => 'plugins/pockets/PocketsPlugin.php',
         'addonKey' => 'pockets',
      )),
      'vanilla\\addons\\pockets\\pocketsmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Addons\\Pockets\\PocketsModel',
         'filePath' => 'plugins/pockets/PocketsModel.php',
         'addonKey' => 'pockets',
      )),
      'pocket' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Pocket',
         'filePath' => 'plugins/pockets/library/Pocket.php',
         'addonKey' => 'pockets',
      )),
      'vanilla\\pockets\\addon\\pocketscontainerrules' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Pockets\\Addon\\PocketsContainerRules',
         'filePath' => 'plugins/pockets/Addon/PocketsContainerRules.php',
         'addonKey' => 'pockets',
      )),
      'pocketsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'PocketsApiController',
         'filePath' => 'plugins/pockets/Controllers/Api/PocketsApiController.php',
         'addonKey' => 'pockets',
      )),
    ),
     'subdir' => '/plugins/pockets',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'PocketsPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
        0 => 'Vanilla\\Pockets\\Addon\\PocketsContainerRules',
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'vanillaconnect' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'VanillaConnect',
      'description' => 'VanillaConnect SSO.',
      'key' => 'vanillaconnect',
      'type' => 'addon',
      'version' => '1.0',
      'settingsUrl' => '/settings/vanillaconnect',
      'usePopupSettings' => false,
      'settingsPermission' => 'Garden.Settings.Manage',
      'hidden' => true,
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Alexandre (DaazKu) Chouinard',
          'email' => 'alexandre.c@vanillaforums.com',
          'homepage' => 'https://github.com/DaazKu',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.6',
      ),
      'oldType' => 'plugin',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'vanilla\\vanillaconnect\\vanillaconnectauthenticator' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\VanillaConnect\\VanillaConnectAuthenticator',
         'filePath' => 'plugins/vanillaconnect/VanillaConnectAuthenticator.php',
         'addonKey' => 'vanillaconnect',
      )),
      'vanilla\\vanillaconnect\\vanillaconnectplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\VanillaConnect\\VanillaConnectPlugin',
         'filePath' => 'plugins/vanillaconnect/VanillaConnectPlugin.php',
         'addonKey' => 'vanillaconnect',
      )),
    ),
     'subdir' => '/plugins/vanillaconnect',
     'translations' => 
    array (
    ),
     'special' => 
    array (
      'plugin' => 'Vanilla\\VanillaConnect\\VanillaConnectPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'multilingual' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Multilingual',
      'description' => 'Allows use of multiple languages. Users can select their preferred language via a link in the footer, and administrators may embed their forum in different languages in different places.',
      'version' => '1.4.1',
      'mobileFriendly' => true,
      'icon' => 'multilingual.png',
      'key' => 'multilingual',
      'type' => 'addon',
      'license' => 'GPL-2.0-only',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Lincoln Russell',
          'email' => 'lincoln@vanillaforums.com',
          'homepage' => 'http://lincolnwebs.com',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.5',
      ),
      'oldType' => 'plugin',
      'keyRaw' => 'Multilingual',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'multilingualplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'MultilingualPlugin',
         'filePath' => 'plugins/Multilingual/class.multilingual.plugin.php',
         'addonKey' => 'multilingual',
      )),
      'localechoosermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'LocaleChooserModule',
         'filePath' => 'plugins/Multilingual/modules/class.localechoosermodule.php',
         'addonKey' => 'multilingual',
      )),
    ),
     'subdir' => '/plugins/Multilingual',
     'translations' => 
    array (
    ),
     'special' => 
    array (
      'plugin' => 'MultilingualPlugin',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
  'reactions' => 
  \Vanilla\Addon::__set_state(array(
     'info' => 
    array (
      'name' => 'Reactions',
      'description' => 'Adds reaction options to discussions & comments.',
      'version' => '1.4.6',
      'registerPermissions' => 
      array (
        'Reactions.Positive.Add' => 'Garden.SignIn.Allow',
        'Reactions.Negative.Add' => 'Garden.SignIn.Allow',
        'Reactions.Flag.Add' => 'Garden.SignIn.Allow',
      ),
      'mobileFriendly' => true,
      'settingsUrl' => '/reactions/settings',
      'icon' => 'reactions.png',
      'key' => 'reactions',
      'type' => 'addon',
      'documentationUrl' => 'http://docs.vanillaforums.com/help/reputation/reactions/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Todd Burry',
          'email' => 'todd@vanillaforums.com',
          'homepage' => 'http://www.vanillaforums.org/profile/todd',
        ),
      ),
      'require' => 
      array (
        'vanilla' => '>=2.4',
      ),
      'oldType' => 'plugin',
      'keyRaw' => 'Reactions',
      'Issues' => 
      array (
      ),
      'priority' => 100,
    ),
     'classes' => 
    array (
      'reactionsfilteropenapi' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionsFilterOpenApi',
         'filePath' => 'plugins/Reactions/ReactionsFilterOpenApi.php',
         'addonKey' => 'reactions',
      )),
      'bestoffiltermodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'BestOfFilterModule',
         'filePath' => 'plugins/Reactions/BestOfFilterModule.php',
         'addonKey' => 'reactions',
      )),
      'reactionmodel' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionModel',
         'filePath' => 'plugins/Reactions/ReactionModel.php',
         'addonKey' => 'reactions',
      )),
      'reactionsplugin' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionsPlugin',
         'filePath' => 'plugins/Reactions/ReactionsPlugin.php',
         'addonKey' => 'reactions',
      )),
      'reactionscontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionsController',
         'filePath' => 'plugins/Reactions/controllers/ReactionsController.php',
         'addonKey' => 'reactions',
      )),
      'reactionsapicontroller' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionsApiController',
         'filePath' => 'plugins/Reactions/controllers/api/ReactionsApiController.php',
         'addonKey' => 'reactions',
      )),
      'usertag' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'UserTag',
         'filePath' => 'plugins/Reactions/models/UserTagModel.php',
         'addonKey' => 'reactions',
      )),
      'vanilla\\reactions\\models\\reactionsquicklinksprovider' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Reactions\\Models\\ReactionsQuickLinksProvider',
         'filePath' => 'plugins/Reactions/models/ReactionsQuickLinksProvider.php',
         'addonKey' => 'reactions',
      )),
      'reactionsmodule' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'ReactionsModule',
         'filePath' => 'plugins/Reactions/modules/ReactionsModule.php',
         'addonKey' => 'reactions',
      )),
      'vanilla\\reactions\\addon\\reactionbadges' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Reactions\\Addon\\ReactionBadges',
         'filePath' => 'plugins/Reactions/Addon/ReactionBadges.php',
         'addonKey' => 'reactions',
      )),
      'vanilla\\reactions\\addon\\reactionscontainerrules' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Reactions\\Addon\\ReactionsContainerRules',
         'filePath' => 'plugins/Reactions/Addon/ReactionsContainerRules.php',
         'addonKey' => 'reactions',
      )),
      'vanilla\\reactions\\events\\reactionevent' => 
      \Vanilla\Utility\PhpClassParsed::__set_state(array(
         'className' => 'Vanilla\\Reactions\\Events\\ReactionEvent',
         'filePath' => 'plugins/Reactions/Events/ReactionEvent.php',
         'addonKey' => 'reactions',
      )),
    ),
     'subdir' => '/plugins/Reactions',
     'translations' => 
    array (
      'en' => 
      array (
        0 => '/locale/en.php',
      ),
    ),
     'special' => 
    array (
      'plugin' => 'ReactionsPlugin',
      'bootstrap' => '/bootstrap.php',
    ),
     'specialClasses' => 
    \Vanilla\AddonSpecialClasses::__set_state(array(
       'structureClasses' => 
      array (
      ),
       'containerRulesClasses' => 
      array (
        0 => 'Vanilla\\Reactions\\Addon\\ReactionsContainerRules',
      ),
       'addonConfigurationClasses' => 
      array (
      ),
       'eventHandlersClasses' => 
      array (
      ),
    )),
  )),
);
