<?php return \Vanilla\Web\Asset\WebpackAssetDefinitionCollection::__set_state(array(
   'section' => 'admin',
   'jsAssetsByAddonKey' => 
  array (
    'dashboard' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/addons/dashboard-common.761f8414839fd094059a.min.js',
         'assetType' => 'js',
         'section' => 'admin',
         'addonKey' => 'dashboard',
      )),
    ),
    'vanilla' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/addons/vanilla.c3dcc2f6d417416474e2.min.js',
         'assetType' => 'js',
         'section' => 'admin',
         'addonKey' => 'vanilla',
      )),
    ),
    'oauth2' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/addons/oauth2.829fe045d95393b2fdce.min.js',
         'assetType' => 'js',
         'section' => 'admin',
         'addonKey' => 'oauth2',
      )),
    ),
    'pockets' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/addons/pockets.483751dc7b5a5013fa9a.min.js',
         'assetType' => 'js',
         'section' => 'admin',
         'addonKey' => 'pockets',
      )),
    ),
    'rich-editor' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/addons/rich-editor.d3c37011df688255a5a0.min.js',
         'assetType' => 'js',
         'section' => 'admin',
         'addonKey' => 'rich-editor',
      )),
    ),
    'swagger-ui' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/addons/swagger-ui.6b46b54784358a7f47ba.min.js',
         'assetType' => 'js',
         'section' => 'admin',
         'addonKey' => 'swagger-ui',
      )),
    ),
  ),
   'cssAssetsByAddonKey' => 
  array (
    'dashboard' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/4fbac7f3c52db3b7798c.min.css',
         'assetType' => 'css',
         'section' => 'admin',
         'addonKey' => 'dashboard',
      )),
    ),
    'pockets' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/admin/async/f22f4259ea853c5674fd.min.css',
         'assetType' => 'css',
         'section' => 'admin',
         'addonKey' => 'pockets',
      )),
    ),
  ),
   'jsAssetsGlobal' => 
  array (
    0 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/packages.addcd601f2d4be40c4d3.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    1 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/vendors.28b69543eaf5ffa64df9.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    2 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-e2ae3a23.e7892331c31639eba966.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    3 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/775.f402fd8d0685efd95657.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    4 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-cc102d41.0248d2bbb46f3c59e8ad.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    5 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-de8b5b60.faec119a443f2f31aba5.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    6 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-6b9cbaae.d459673ec5788bb507c1.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    7 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-10c31700.6a9024d7cf727a4de6a3.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    8 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-1b22c49f.f7ef545883c4d19e4864.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    9 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-2c9e96e5.ced90d01de865672db40.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    10 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-9aa6fe4f.bd930ee0b747d588c691.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    11 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-c19d9c43.42fd5f10f9329ff4306e.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    12 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-c7c1a4dd.f4bbd35e8af77ff3c49c.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    13 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-f130b804.03e3380575f34076008c.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    14 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-3b962b3c.3cde1183bb1ac0f3f661.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    15 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-91c7739a.bb7dad1933a928469b75.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    16 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-1b5f44f5.5148f0ea2cb7d8cc91bc.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    17 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-bd952247.f04bcf846981be465ac8.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    18 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-2da6cbf1.7a5d3ee4d651339c855b.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    19 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-3f9d9fa6.72f78ce641cb1eed7e28.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    20 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-cd35722a.dd146dd2489cc75d0bb6.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    21 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-b40b2b97.7108127cf4c01889c742.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    22 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-1f0b08b7.23e414b11f93f511e659.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    23 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-4774b20b.36363e45be9b8a89289f.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    24 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-78c5182d.23d90c6d761cc3379704.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
    25 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/admin/library-d51a67ca.ec30f08abfcc360ee9ef.min.js',
       'assetType' => 'js',
       'section' => 'admin',
       'addonKey' => NULL,
    )),
  ),
   'cssAssetsGlobal' => NULL,
   'runtimeJsAsset' => 
  \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
     'assetPath' => '/dist/v1/admin/runtime.5e46cf5eee3531e20c62.min.js',
     'assetType' => 'js',
     'section' => 'admin',
     'addonKey' => NULL,
  )),
   'bootstrapJsAsset' => 
  \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
     'assetPath' => '/dist/v1/admin/bootstrap.451d829d06a0561949c7.min.js',
     'assetType' => 'js',
     'section' => 'admin',
     'addonKey' => NULL,
  )),
   'allAssetUrls' => 
  array (
    '/dist/v1/admin/async/addons/dashboard-common.761f8414839fd094059a.min.js' => true,
    '/dist/v1/admin/async/4fbac7f3c52db3b7798c.min.css' => true,
    '/dist/v1/admin/async/addons/vanilla.c3dcc2f6d417416474e2.min.js' => true,
    '/dist/v1/admin/async/addons/oauth2.829fe045d95393b2fdce.min.js' => true,
    '/dist/v1/admin/async/f22f4259ea853c5674fd.min.css' => true,
    '/dist/v1/admin/async/addons/pockets.483751dc7b5a5013fa9a.min.js' => true,
    '/dist/v1/admin/async/addons/rich-editor.d3c37011df688255a5a0.min.js' => true,
    '/dist/v1/admin/async/addons/swagger-ui.6b46b54784358a7f47ba.min.js' => true,
    '/dist/v1/admin/packages.addcd601f2d4be40c4d3.min.js' => true,
    '/dist/v1/admin/vendors.28b69543eaf5ffa64df9.min.js' => true,
    '/dist/v1/admin/library-e2ae3a23.e7892331c31639eba966.min.js' => true,
    '/dist/v1/admin/775.f402fd8d0685efd95657.min.js' => true,
    '/dist/v1/admin/library-cc102d41.0248d2bbb46f3c59e8ad.min.js' => true,
    '/dist/v1/admin/library-de8b5b60.faec119a443f2f31aba5.min.js' => true,
    '/dist/v1/admin/library-6b9cbaae.d459673ec5788bb507c1.min.js' => true,
    '/dist/v1/admin/library-10c31700.6a9024d7cf727a4de6a3.min.js' => true,
    '/dist/v1/admin/library-1b22c49f.f7ef545883c4d19e4864.min.js' => true,
    '/dist/v1/admin/library-2c9e96e5.ced90d01de865672db40.min.js' => true,
    '/dist/v1/admin/library-9aa6fe4f.bd930ee0b747d588c691.min.js' => true,
    '/dist/v1/admin/library-c19d9c43.42fd5f10f9329ff4306e.min.js' => true,
    '/dist/v1/admin/library-c7c1a4dd.f4bbd35e8af77ff3c49c.min.js' => true,
    '/dist/v1/admin/library-f130b804.03e3380575f34076008c.min.js' => true,
    '/dist/v1/admin/library-3b962b3c.3cde1183bb1ac0f3f661.min.js' => true,
    '/dist/v1/admin/library-91c7739a.bb7dad1933a928469b75.min.js' => true,
    '/dist/v1/admin/library-1b5f44f5.5148f0ea2cb7d8cc91bc.min.js' => true,
    '/dist/v1/admin/library-bd952247.f04bcf846981be465ac8.min.js' => true,
    '/dist/v1/admin/library-2da6cbf1.7a5d3ee4d651339c855b.min.js' => true,
    '/dist/v1/admin/library-3f9d9fa6.72f78ce641cb1eed7e28.min.js' => true,
    '/dist/v1/admin/library-cd35722a.dd146dd2489cc75d0bb6.min.js' => true,
    '/dist/v1/admin/library-b40b2b97.7108127cf4c01889c742.min.js' => true,
    '/dist/v1/admin/library-1f0b08b7.23e414b11f93f511e659.min.js' => true,
    '/dist/v1/admin/library-4774b20b.36363e45be9b8a89289f.min.js' => true,
    '/dist/v1/admin/library-78c5182d.23d90c6d761cc3379704.min.js' => true,
    '/dist/v1/admin/library-d51a67ca.ec30f08abfcc360ee9ef.min.js' => true,
    '/dist/v1/admin/bb1e2bc6a58e617ce856.png' => true,
    '/dist/v1/admin/57acba26f618ac90800e50d6ae79837e.svg' => true,
    '/dist/v1/admin/6d2e8a03ddbd49d0cf97d30fc8d347e8.svg' => true,
    '/dist/v1/admin/96e8c181d16d97c060ce7a1b5169c0db.svg' => true,
    '/dist/v1/admin/fd4148c6c6c4dda3a4aad1a8f5df0f89.svg' => true,
    '/dist/v1/admin/bddd9ea001628d10e9a8b7b2fde173f6.svg' => true,
    '/dist/v1/admin/619bc298cfc2a42ba6427ad2a10a8858.svg' => true,
    '/dist/v1/admin/eec1ec876176df643de362ccdc267e2e.svg' => true,
  ),
));
