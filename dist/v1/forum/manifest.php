<?php return \Vanilla\Web\Asset\WebpackAssetDefinitionCollection::__set_state(array(
   'section' => 'forum',
   'jsAssetsByAddonKey' => 
  array (
    'dashboard' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/dashboard-common.8608087e1c520c956719.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'dashboard',
      )),
    ),
    'lavendermoon' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/lavendermoon.41e0a5572823fa5cb945.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'lavendermoon',
      )),
    ),
    'lavendersun' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/lavendersun.bea75ef433487248ca7c.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'lavendersun',
      )),
    ),
    'theme-foundation' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/theme-foundation.0c82643732a84f1278c5.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'theme-foundation',
      )),
    ),
    'qna' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/qna.ad9f23bcbfafd385c058.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'qna',
      )),
    ),
    'reactions' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/reactions.58cb1f4faae416b24a59.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'reactions',
      )),
    ),
    'rich-editor' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/addons/rich-editor.67ed08074d97a1a0025b.min.js',
         'assetType' => 'js',
         'section' => 'forum',
         'addonKey' => 'rich-editor',
      )),
    ),
  ),
   'cssAssetsByAddonKey' => 
  array (
    'vanilla' => 
    array (
      0 => 
      \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
         'assetPath' => '/dist/v1/forum/async/5af150dc6bc951f9dbaf.min.css',
         'assetType' => 'css',
         'section' => 'forum',
         'addonKey' => 'vanilla',
      )),
    ),
  ),
   'jsAssetsGlobal' => 
  array (
    0 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/packages.dbeb6f186a6ef8d31ccb.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    1 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/vendors.7beb6ddea88aea9efb44.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    2 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-2d32d187.352946159bcb9c2a6c73.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    3 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/1312.063d470bdf80b8f0a63f.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    4 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-cc102d41.8d2cc9de47b5e85598b0.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    5 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-de8b5b60.545063d18aa8f391ccc9.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    6 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-6b9cbaae.3745282a3205fef02667.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    7 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-155ed1d1.9883ebd74261e094d12c.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    8 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-b3ff4ff5.19a97a764697b6dc7e53.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    9 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-b9e9f021.9536c365ea32bd884d55.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    10 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-10c31700.0afc8f92ba97c7b31482.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    11 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-620da1e1.8e1396aa60b9cb9d9cb9.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    12 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-2c9e96e5.5eb6ad891323c5962b4d.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    13 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-154f14ca.ad65460022a08340d2b0.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    14 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-222bc760.c879890eb1ce7897269d.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    15 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-a48e544a.4fd05c9f0da2625b23fe.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    16 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-d4f2673a.010fa1e4f52e78085687.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    17 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-b5206d0f.b3ea2fa5a64f776d0bef.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    18 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-2f1652fc.3be6df4dd288002db1a6.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    19 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-64840acf.2e329d6a5f339522d723.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    20 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-9aa6fe4f.144a66511319e77a1e00.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    21 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-c19d9c43.864fb708a085b75f7d1c.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    22 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-c7c1a4dd.256b9a70f5230e230019.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    23 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-8351907e.ffcfd4a5ddf659956052.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    24 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-3b962b3c.c514a57509c92e6384e4.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    25 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-91c7739a.9f7d017d2cf2073b7f94.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    26 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-0a00c317.03f48b575b32e5f57b37.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    27 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-125fb5ae.92ec16f53e5e23de45ac.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    28 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-1b5f44f5.88048884abb6dbc3e187.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    29 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-bd952247.74028765947b026b89e2.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    30 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-2da6cbf1.3fe0628469f37d4e62a0.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    31 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-3f9d9fa6.20595974a11e65ac8027.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    32 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-b40b2b97.f0264632ff8a41d92e38.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    33 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-1e3a4ead.2657aaf2a77785b1f08d.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    34 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-1f0b08b7.735171275a45c9641ce2.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    35 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-4774b20b.917dd8d0886cb003db95.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    36 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-fae66388.8b49f1b5b31383f28b61.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    37 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-35a1f0cc.3c194327338df63a52fa.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    38 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-d51a67ca.d6cfdcfd513d8182e2df.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    39 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-70109b0c.3fec959b8a8da5b0333b.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
    40 => 
    \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
       'assetPath' => '/dist/v1/forum/library-00a511ae.ff5e526c61a50f11a8f0.min.js',
       'assetType' => 'js',
       'section' => 'forum',
       'addonKey' => NULL,
    )),
  ),
   'cssAssetsGlobal' => NULL,
   'runtimeJsAsset' => 
  \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
     'assetPath' => '/dist/v1/forum/runtime.6d9994eec8a2bf110528.min.js',
     'assetType' => 'js',
     'section' => 'forum',
     'addonKey' => NULL,
  )),
   'bootstrapJsAsset' => 
  \Vanilla\Web\Asset\WebpackAssetDefinition::__set_state(array(
     'assetPath' => '/dist/v1/forum/bootstrap.4d73d2d79d6794f1ef89.min.js',
     'assetType' => 'js',
     'section' => 'forum',
     'addonKey' => NULL,
  )),
   'allAssetUrls' => 
  array (
    '/dist/v1/forum/async/addons/dashboard-common.8608087e1c520c956719.min.js' => true,
    '/dist/v1/forum/async/addons/lavendermoon.41e0a5572823fa5cb945.min.js' => true,
    '/dist/v1/forum/async/5af150dc6bc951f9dbaf.min.css' => true,
    '/dist/v1/forum/async/addons/lavendersun.bea75ef433487248ca7c.min.js' => true,
    '/dist/v1/forum/async/addons/theme-foundation.0c82643732a84f1278c5.min.js' => true,
    '/dist/v1/forum/async/addons/qna.ad9f23bcbfafd385c058.min.js' => true,
    '/dist/v1/forum/async/addons/reactions.58cb1f4faae416b24a59.min.js' => true,
    '/dist/v1/forum/async/addons/rich-editor.67ed08074d97a1a0025b.min.js' => true,
    '/dist/v1/forum/packages.dbeb6f186a6ef8d31ccb.min.js' => true,
    '/dist/v1/forum/vendors.7beb6ddea88aea9efb44.min.js' => true,
    '/dist/v1/forum/library-2d32d187.352946159bcb9c2a6c73.min.js' => true,
    '/dist/v1/forum/1312.063d470bdf80b8f0a63f.min.js' => true,
    '/dist/v1/forum/library-cc102d41.8d2cc9de47b5e85598b0.min.js' => true,
    '/dist/v1/forum/library-de8b5b60.545063d18aa8f391ccc9.min.js' => true,
    '/dist/v1/forum/library-6b9cbaae.3745282a3205fef02667.min.js' => true,
    '/dist/v1/forum/library-155ed1d1.9883ebd74261e094d12c.min.js' => true,
    '/dist/v1/forum/library-b3ff4ff5.19a97a764697b6dc7e53.min.js' => true,
    '/dist/v1/forum/library-b9e9f021.9536c365ea32bd884d55.min.js' => true,
    '/dist/v1/forum/library-10c31700.0afc8f92ba97c7b31482.min.js' => true,
    '/dist/v1/forum/library-620da1e1.8e1396aa60b9cb9d9cb9.min.js' => true,
    '/dist/v1/forum/library-2c9e96e5.5eb6ad891323c5962b4d.min.js' => true,
    '/dist/v1/forum/library-154f14ca.ad65460022a08340d2b0.min.js' => true,
    '/dist/v1/forum/library-222bc760.c879890eb1ce7897269d.min.js' => true,
    '/dist/v1/forum/library-a48e544a.4fd05c9f0da2625b23fe.min.js' => true,
    '/dist/v1/forum/library-d4f2673a.010fa1e4f52e78085687.min.js' => true,
    '/dist/v1/forum/library-b5206d0f.b3ea2fa5a64f776d0bef.min.js' => true,
    '/dist/v1/forum/library-2f1652fc.3be6df4dd288002db1a6.min.js' => true,
    '/dist/v1/forum/library-64840acf.2e329d6a5f339522d723.min.js' => true,
    '/dist/v1/forum/library-9aa6fe4f.144a66511319e77a1e00.min.js' => true,
    '/dist/v1/forum/library-c19d9c43.864fb708a085b75f7d1c.min.js' => true,
    '/dist/v1/forum/library-c7c1a4dd.256b9a70f5230e230019.min.js' => true,
    '/dist/v1/forum/library-8351907e.ffcfd4a5ddf659956052.min.js' => true,
    '/dist/v1/forum/library-3b962b3c.c514a57509c92e6384e4.min.js' => true,
    '/dist/v1/forum/library-91c7739a.9f7d017d2cf2073b7f94.min.js' => true,
    '/dist/v1/forum/library-0a00c317.03f48b575b32e5f57b37.min.js' => true,
    '/dist/v1/forum/library-125fb5ae.92ec16f53e5e23de45ac.min.js' => true,
    '/dist/v1/forum/library-1b5f44f5.88048884abb6dbc3e187.min.js' => true,
    '/dist/v1/forum/library-bd952247.74028765947b026b89e2.min.js' => true,
    '/dist/v1/forum/library-2da6cbf1.3fe0628469f37d4e62a0.min.js' => true,
    '/dist/v1/forum/library-3f9d9fa6.20595974a11e65ac8027.min.js' => true,
    '/dist/v1/forum/library-b40b2b97.f0264632ff8a41d92e38.min.js' => true,
    '/dist/v1/forum/library-1e3a4ead.2657aaf2a77785b1f08d.min.js' => true,
    '/dist/v1/forum/library-1f0b08b7.735171275a45c9641ce2.min.js' => true,
    '/dist/v1/forum/library-4774b20b.917dd8d0886cb003db95.min.js' => true,
    '/dist/v1/forum/library-fae66388.8b49f1b5b31383f28b61.min.js' => true,
    '/dist/v1/forum/library-35a1f0cc.3c194327338df63a52fa.min.js' => true,
    '/dist/v1/forum/library-d51a67ca.d6cfdcfd513d8182e2df.min.js' => true,
    '/dist/v1/forum/library-70109b0c.3fec959b8a8da5b0333b.min.js' => true,
    '/dist/v1/forum/library-00a511ae.ff5e526c61a50f11a8f0.min.js' => true,
    '/dist/v1/forum/619bc298cfc2a42ba6427ad2a10a8858.svg' => true,
  ),
));
