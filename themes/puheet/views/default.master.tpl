<!DOCTYPE html>
<html lang="{$CurrentLocale.Key}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {asset name="Head"}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,400&display=swap" rel="stylesheet">
</head>

{assign var="SectionGroups" value=(isset($Groups) || isset($Group))}

<body id="{$BodyID}" class="
    {$BodyClass}

    {if $User.SignedIn}
        UserLoggedIn
    {else}
        UserLoggedOut
    {/if}

    {if inSection('Discussion') and $Page gt 1}
        isNotFirstPage
    {/if}

    {if inSection('Group') && !isset($Group.Icon)}
        noGroupIcon
    {/if}

    {if $Homepage}
        HomePage
    {/if}

    locale-{$CurrentLocale.Lang}
">

    <!--[if lt IE 9]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="Frame" id="page">
        <div class="Frame-top">
            <div class="Frame-header">
                {include file="partials/header.tpl"}
            </div>
            <div class="Frame-body">
                <div class="Frame-content">
                    <div>
                        <div>
                            <div class="Frame-details">
                                {if !$isHomepage}
                                    <div class="Frame-row Container Frame-contentWrap">
                                        <nav class="BreadcrumbsBox">
                                            {breadcrumbs}
                                        </nav>
                                    </div>
                                {/if}
                                <div class="Frame-row SearchBoxMobile">
                                    {if !$SectionGroups && !inSection(["SearchResults"])}
                                        <div class="SearchBox js-sphinxAutoComplete" role="search">
                                            {module name="AdvancedSearchModule"}
                                        </div>
                                    {/if}
                                </div>

                                {if $Homepage}
                                    {include file="partials/home_page_banner.tpl"}
                                    <div class="Frame-row frame-row-featured-categories">
                                        {include file="partials/featured_categories.tpl"}
                                    </div>
                                    <div class="Frame-row Container Frame-contentWrap" style="margin-bottom: 40px;">
                                        {include file="partials/featured_discussions.tpl"}
                                    </div>
                                {/if}

                                {if isset($isCategoryGalleryMainPage) && $isCategoryGalleryMainPage}
                                    <div class="Frame-row Container Frame-contentWrap">
                                        {include file="partials/category_gallery.tpl"}
                                    </div>
                                {/if}
                                <div class="Frame-row Container Frame-contentWrap">
                                    <main class="Content MainContent">
                                        {if inSection("Profile")}
                                            <div class="Profile-header">
                                                <div class="Profile-photo">
                                                    <div class="PhotoLarge">
                                                        {module name="UserPhotoModule"}
                                                    </div>
                                                </div>
                                                <div class="Profile-name">
                                                    <h1 class="Profile-username">
                                                        {$Profile.Name|escape:'html'}
                                                    </h1>
                                                    {if isset($Rank)}
                                                        <span class="Profile-rank">{$Rank.Label|escape:'html'}</span>
                                                    {/if}
                                                </div>
                                            </div>
                                        {/if}
                                        {if $Homepage}
                                            <h1 class="latest-discussions">{t c="Latest discussions"}</h1>
                                            <style>.H.HomepageTitle { display: none; }</style>
                                        {/if}
                                        {asset name="Content"}
                                    </main>
                                    <aside class="Panel Panel-main">
                                        {if !$IsSignedIn}
                                          {include file="partials/magic_link_form.tpl"}
                                        {/if}
                                        {* Searchbox is not wanted as part of "default" community
                                        Decided in daily 8.6.2021
                                        {if !$SectionGroups}
                                            <div class="SearchBox js-sphinxAutoComplete" role="search">
                                                {searchbox}
                                            </div>
                                        {/if}
                                        *}
                                        {asset name="Panel"}
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Frame-footer">
            {include file="partials/footer.tpl"}
        </div>
    </div>
    <div id="modals"></div>
    {event name="AfterBody"}
</body>
</html>
