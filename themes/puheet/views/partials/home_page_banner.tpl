{foreach from=$HomePageBanners item=banner name=banner}
<div id="home-herobanner">
    <a href="{$banner['Href']}">
        <img
            src="{$banner['Desktop']}"
            alt=""
            role="presentation"
            height="600px"
            width="100%"
        >
        <img
            src="{$banner['Tablet']}"
            alt=""
            role="presentation"
            height="600px"
            width="100%"
        >
        <img
            src="{$banner['Mobile']}"
            alt=""
            role="presentation"
            height="600px"
            width="100%"
        >
    </a>
</div>
{/foreach}

<style>
/* Herobanner */
@media screen and (min-width: 950px) {

    #home-herobanner img:first-child {
        object-fit: cover;
    }

    #home-herobanner img:nth-child(2),
    #home-herobanner img:nth-child(3) {
        display: none;
    }
}

@media screen and (min-width: 540px) and (max-width: 949px) {

    #home-herobanner img:first-child,
    #home-herobanner img:nth-child(3) {
        display: none;
    }

    #home-herobanner img:nth-child(2) {
        object-fit: cover;
    }
}

@media screen and (max-width: 539px) {

    #home-herobanner img:first-child,
    #home-herobanner img:nth-child(2) {
        display: none;
    }

    #home-herobanner img:nth-child(3) {
        object-fit: cover;
    }
}
</style>
