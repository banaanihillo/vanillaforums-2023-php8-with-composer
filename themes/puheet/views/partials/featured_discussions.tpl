
<div class="featured-discussions-container">
<h1 class="featured-discussions-title">{t c="Featured discussions"}</h1>
<div class="featured-discussions-items">

{foreach from=$FeaturedDiscussions item=discussion name=discussion}
    <div class="featured-discussions-item item{$smarty.foreach.discussion.index}" onclick="location.href='{$discussion['Url']}'">
        <h4 class="item-title">{$discussion['Name']}</h4>
        {if $discussion['Tag']}
          <div class="item-tag">{$discussion['Tag']}</div>
        {/if}
    </div>
{/foreach}
</div>
</div>
<div style="margin-top: 40px"> </div>

<style>
  .featured-discussions-container {
    width: 100%;
  }
  .featured-discussions-title {
    margin: 40px 0;
    text-align: center;
  }  

  .featured-discussions-items {
    display: grid;
    grid-gap: 20px;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    justify-content: space-between;
    /*height: 280px;*/
    filter: drop-shadow(0px 4px 4px rgba(128, 60, 161, 0.25));
  }

  .featured-discussions-item {
    min-height: 200px;
    position: relative;
    display: block;
    transition: transform .2s;
    cursor: pointer;
  }

  .featured-discussions-item:before {
    content: " ";
    background-repeat: no-repeat;
    background-position: 50% 0;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: 0;
    border-radius: 24px;
  }

  {foreach from=$FeaturedDiscussions item=discussion name=discussion}
  .featured-discussions-item.item{$smarty.foreach.discussion.index}:before {
    background: /*linear-gradient(90deg, rgba(5,19,168,0.4) 0%, rgba(5,19,168,0.4) 100%),*/ url('{$discussion['PhotoUrl']}') no-repeat 50% 0;
    -ms-background-size: cover;
    -o-background-size: cover;
    -moz-background-size: cover;
    -webkit-background-size: cover;
    background-size: cover;
  }
  {/foreach}

  .featured-discussions-item .item-title {
    font-size: 22px;
    font-weight: 300;
    position: relative;
    z-index: 2;
    color: #33213D;
    margin-top: 240px;
    padding: 24px;
    text-align: center;
  }

  .featured-discussions-item:hover:before {
    opacity: 0.7;
  }

  .featured-discussions-item .item-tag {
    text-shadow: 0 0 10px rgba(0,0,0,0.55);
    box-shadow: 0px 0px 4px rgba(0,0,0,0.4);
    text-transform: uppercase;
    color: white;
    font-weight: 600;
    padding: 4px 16px;
    font-size: 16px;
    border-radius: 16px;
    border: 2px solid white;
    position: relative;
    top: 30px;
    display: inline-block;
  }

  @media only screen and (max-width: 768px) {
    .featured-discussions-item.last {
      display: none;
    }
  }

</style>
