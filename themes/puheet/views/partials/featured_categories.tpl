<div class="featured-categories-container">
  {* Topics title --> *}
  <h1 class="featured-categories-title Container">{t c="Topics"}</h1>
  <div class="featured-categories-items">
    {foreach $FeaturedCategories as $category}
      <div
        class="featured-categories-item"
        data-category="{$category['Name']}"
      >
        <img
          class="item-icon"
          src="{$category['PhotoUrl']}"
          alt=""
          role="presentation"
        />
        <h4 class="item-title">
          <a href="{$category['Url']}">
            {$category['Name']}
          </a>
        </h4>
      </div>
    {/foreach}
  </div>
</div>
<style>
  /*"Topics" Featured Categories*/
  .featured-categories-container {
    width: 100%;
  }

  .featured-categories-title {
    max-width: 1272px;
    margin: 0 auto 40px;
    text-align: center;
  }

  .featured-categories-items {
    display: flex;
    overflow: auto;
    padding: 10px 0;
    height: 280px;
    max-width: 1272px;
    margin: auto;
  }

  .featured-categories-items::before,
  .featured-categories-items::after {
    content: "";
    padding-right: 12px; /* update .Container class */
  }

  .featured-categories-item {
    flex-shrink: 0;
    width: 250px;
    margin-right: 10px;
    text-align: center;
    height: 250px;
    position: relative;
    display: block;
    background-color: #FFFFFF;
    cursor: pointer;
    border-radius: 24px;
    /*box-shadow: 0 1px 2px 0 #bababa;*/
    padding-top: 16px;
  }
  
  .featured-categories-item:last-child {
    margin-right: 0;
  }

  .featured-categories-item .item-title{
    margin: 20px 0 20px 0;
    padding-bottom: 30px;
    color: #33213D;
    font-weight: 300;
    font-size: 26px;
  }

  .featured-categories-item .item-icon {
    width: 160px;
    height: 160px;
    object-fit: fill;
  }

  .featured-categories-item:hover {
    border-color: #c77827;
    transition: transform .3s;
    transform: scale(1.04);
  }

  .featured-categories-tag {
    text-transform: uppercase;
    color: white;
    font-weight: 600;
    padding: 4px 16px;
    font-size: 14px;
    border-radius: 16px;
    border: 2px solid white;
    position: relative;
    top: 35px;
  }

    {* Horizontal scroll scrollbar invisible *}
  .featured-categories-items::-webkit-scrollbar {
    display: none;
  }

  @media only screen and (min-width: 870px) {

    .featured-categories-items {
    justify-content: space-between;
    }

  }  

  @media only screen and (max-width: 786px) {
    .Frame-row.frame-row-featured-categories {
      margin-top: 20px;
    }

    .featured-categories-title {
      margin: 0 auto 25px;
    }

  }
 

</style>
