<?php

class PuheetPlatformIntegrationPlugin extends Gdn_Plugin
{
    /**
     * Usage: $this->log("yourMessageHere $variableName also supported");
     */
    private function log($logMessage)
    {
        error_log("$logMessage\n", 3, '/var/www/forum/log/DebugLog');
        return;
    }

    /**
     * Forces session validation check to Puheet backend when navigating inside the UI.
     *
     * @param $caller
     * @param $args
     * @throws Exception
     */
    public function gdn_dispatcher_beforeAnalyzeRequest_handler($sender, $args)
    {
        $reqPath = $args["Request"]->getPath();

        if (
            // Filters out paths etc. that do not need to validate Puheet session
            strpos($reqPath, "api/v2/") === 1 || // all API paths
            strpos($reqPath, "entry/signout") === 1 || // sign out
            strpos($reqPath, "entry/signin") === 1 || // sign in // TODO does this break normal flow?
            strpos($reqPath, "puheet/") === 1 || // this plugin's endpoints
            !$this->isConfigured() // don't check session if this plugin hasn't been configured properly!

        ) {
            return;
        }
        $this->validateSession($reqPath);
    }


    /**
     * Validates the user's session according to these rules:
     * - User has an active Puheet session and is signed in to Vanilla - checks signed in Vanilla user corresponds to
     *   Puheet session data.
     * - User has an active Puheet session and is NOT signed in to Vanilla - signs the user in to Vanilla, or, if user
     *   has no Vanilla account, registers a new account and the signs them in.
     * - User doesn't have an active Puheet session and is signed in to Vanilla - signs the user out of Vanilla.
     * - User doesn't have an active Puheet session and is NOT signed in to Vanilla - no action.
     *
     * @param string $path where to redirect after authenticating with an external API
     * @throws Exception
     */
    private function validateSession($path = "")
    {
        if (!empty($_COOKIE["puheet_info"]) && !empty($_COOKIE["puheet_session"])) {
            if (!Gdn::session()->UserID) {
                $this->signIn(); // Not signed in to Vanilla yet - find the correct user and sign them in
            } else {
                $this->validateCurrentUser($path); // Signed in to Vanilla - check if Puheet session matches Vanilla user
            }
        } else if ($this->externalIamIsEnabled()) {
            $this->startExternalAuthentication($path);
        } else if (Gdn::session()->UserID) {
            // No Puheet session cookies and the user has an active Vanilla session
            Gdn::session()->end();
        }

    }

    /**
     * Redirects the user to Puheet backend, which then takes care of authenticating the user with a customer's
     * external IAM.
     * @param $path - the Vanilla path to which to take the user after authentication
     */
    private function startExternalAuthentication($path)
    {
        $userNotSignedInToExternalIam = Gdn::cache()->get(
            "not_signed_in_"
            // May or may not actually rely on the empty string?
            . ($_COOKIE["puheet_session"] ?? "")
        );

        if (isset($_COOKIE["puheet_session"])) {
            // If the program gets here the user has communicated with the backend.
            // Let's assume the user has not signed in,
            // and store a not_signed_in value to the cache,
            // using the puheet_session cookie as an identifier.
            // Then, if the program gets to this method again,
            // it'll notice the user is not signed in
            // and can then terminate the current session
            // and let the user in to the user interface as a guest.
            $this->storeNotSignedIn();
        }

        if ($userNotSignedInToExternalIam) {
            $secure = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']);
            $domainParts = explode(".", $_SERVER["SERVER_NAME"]);
            $domain = substr($_SERVER["SERVER_NAME"], strlen($domainParts[0]));
            $httpOnly = true;

            setcookie("puheet_info", "", 1, "/", $domain, $secure, $httpOnly);
            unset($_COOKIE["puheet_info"]);
            Gdn::session()->end();
            return;
        }

        $target = urlencode("https://" . $_SERVER["SERVER_NAME"] . $path);
        $url = (
            c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl')
            . "/fetch_external_auth?target=$target"
        );
        http_response_code(200);
        header("Location: $url");
        exit();
    }


    /**
     * Stores a temporary identifier to the cache. The identifier means the user is not signed in.
     */
    private function storeNotSignedIn()
    {
        $cacheKey = "not_signed_in_" . $_COOKIE["puheet_session"];
        Gdn::cache()->store(
            $cacheKey,
            true,
            [Gdn_Cache::FEATURE_EXPIRY => 5] // 5 seconds
        );
    }


    /**
     * Sign in user with an active Puheet session to Vanilla.
     * @param string $path where to go after signIn
     * @return void
     * @throws Exception
     */
    private function signIn($path = "")
    {
        $sessionInfo = $this->getPuheetSessionInformation();

        if (empty($sessionInfo)) {
            // No user info was returned by Puheet back-end,
            // i.e. user is not signed in to Puheet platform
            // and should first be signed in to Puheet and then to Vanilla.
            if ($this->externalIamIsEnabled()) {
                return $this->startExternalAuthentication($path);
            }
            return;
        }

        // TODO find the correct user using the UniqueId attribute instead of email
        $userEmail = $sessionInfo->profile->emails[0]->address;
        $vanillaUser = Gdn::userModel()->getWhere(
            ['Email' => $userEmail]
        )->resultArray();

        $forumUserExists = (
            $sessionInfo->forumUser ?? null
            || strlen($sessionInfo?->forumUser?->username ?? "")
        );

        if (count($vanillaUser) === 0 && !$forumUserExists) {
            // User not found = does not exist in Vanilla yet, create new
            $email = $sessionInfo->profile->emails[0]->address;
            $uniqueId = $sessionInfo->profile->id;
            if ($this->externalIamIsEnabled()) {
                $givenName = $sessionInfo->profile->name->givenName;
                $userName = $this->parseGivenName($givenName);
                $this->register($email, $uniqueId, false, $userName);
            } else {
                $this->register($email, $uniqueId);
            }
        } else if (count($vanillaUser) === 0) {
            // User has been removed from Vanilla during the user's active session.
            // This can happen if an admin deletes the user or if the user has requested data deletion.
            // ==> Don't register user to Vanilla, don't sign them in, and tell the backend it should destroy the user's session.
            $this->signOut();
        } else if (count($vanillaUser) > 1) {
            // Query found multiple users
            $error = "Email $userEmail matches multiple users.";
            gdnExceptionHandler(new Exception($error));
            die($error);
        } else {
            // Found one user. To be extra safe, check that their UniqueId matches the profile ID in Puheet session
            $vanillaUserUniqueId = json_decode($vanillaUser[0]["Attributes"])->UniqueID;
            $puheetSessionUniqueId = $sessionInfo->profile->id;

            if ($vanillaUserUniqueId !== $puheetSessionUniqueId && !empty($vanillaUserUniqueId)) {
                // This could happen if a user changes their email. Therefore it would be better to make all user
                // queries with UniqueID instead of email. TODO? Maybe update DB schema and store UniqueID to its own column...?
                $error = "Forum user's ($userEmail) UniqueId doesn't match the ID in Puheet session.";
                gdnExceptionHandler(new Exception($error));
                die($error);
            }

            $userId = $vanillaUser[0]["UserID"];
            $this->cacheSession($userId, $sessionInfo);
            Gdn::session()->start($userId);

            if (empty($vanillaUserUniqueId)) {
                // If the user's UniqueID attribute is somehow empty at this stage, get it from Puheet session and save it to the DB
                $user = Gdn::session();
                Gdn::userModel()->saveAttribute($user->UserID, "UniqueID", $puheetSessionUniqueId);
                $attributes = $user->getAttributes();
                // $attributes->UniqueID = $puheetSessionUniqueId;
                $attributes["UniqueID"] = $puheetSessionUniqueId;
                Gdn::userModel()->setField($user->UserID, 'Attributes', $attributes);
            }
        }
    }

    private function parseGivenName($givenName)
    {
        $name = $givenName;
        if (strpos($name, '.') !== false) {
            $namesArr = explode('.', $name, 2);
            $name = ucfirst($namesArr[0]) . " " . ucfirst($namesArr[1]);
        }
        return $name;
    }

    /**
     * Checks that the user currently signed in to Vanilla is the same as the one who has Puheet session.
     * If not, update Vanilla's session.
     * @throws Exception
     */
    private function validateCurrentUser($path = "")
    {
        $sessionInfo = $this->getPuheetSessionInformation();
        $uniqueId = $sessionInfo ? $sessionInfo->profile->id : -1;
        $vanillaUserUniqueId = Gdn::session()->getAttribute("UniqueID");
        $puheetSessionUniqueId = $uniqueId;

        if ($vanillaUserUniqueId !== $puheetSessionUniqueId) {
            GDN::session()->end();
            $this->signIn($path);
        }
    }


    /**
     * Retrieves session information from Puheet backend.
     * @return mixed
     */
    private function getPuheetSessionInformation()
    {
        $userId = Gdn::session()->UserID;
        $cacheKey = $this->getCacheKey($userId);
        $cachedSessionInfo = Gdn::cache()->get($cacheKey);

        if ($cachedSessionInfo !== Gdn_Cache::CACHEOP_FAILURE) {
            // Returning Puheet session info from cache
            return $cachedSessionInfo;
        }

        $url = c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl') . "/session";
        $headers = [];
        $sessionInfoJson = $this->request($url, $headers);
        $sessionInfo = json_decode($sessionInfoJson);

        if ($sessionInfo === false) {
            return null;
        }

        if (
          isset($sessionInfo->action)
          && $sessionInfo->action === "verify_email"
        ) {
            // User needs to verify their email before they can do anything else
            $verificationUrl = $sessionInfo->url;
            if (headers_sent()) {
                die('<script type="text/javascript">window.location=\'' . $verificationUrl . '\';</script>');
            } else {
                header('Location: ' . $verificationUrl);
                die();
            }
        }

        if ($userId !== 0) {
            $this->cacheSession($userId, $sessionInfo);
        }

        return $sessionInfo;
    }

    private function getCacheKey($userId)
    {
        return "ExternallyLoggedIn_" . $userId;
    }


    /**
     * Saves Puheet session info to cache.
     * @param $userId
     * @param $sessionInfo
     */
    private function cacheSession($userId, $sessionInfo)
    {
        $cacheKey = $this->getCacheKey($userId);
        Gdn::cache()->store(
            $cacheKey,
            $sessionInfo,
            [Gdn_Cache::FEATURE_EXPIRY => 5]
        );
    }


    /**
     * Registers a user with an active Puheet session to Vanilla.
     * @param $email
     * @param $uniqueId
     * @param bool $isCalledThroughApi
     * @return bool|int|string
     * @throws Exception
     */
    public function register(
      $email,
      $uniqueId,
      $isCalledThroughApi = false,
      $userName = ""
    )
    {
        $userData = [];
        $userData['Email'] = $email;
        $userData['Attributes']['UniqueID'] = $uniqueId;
        // $userData['Name'] = $this->createUsername();
        if ($userName !== "") {
            $userData['Name'] = $userName;
        } else {
            $customerName = c('Plugins.PuheetPlatformIntegration.CustomerName');
            // User name may or may not be working for saha-sync;
            // use the firstname.lastname unique ID as Firstname Lastname
            if (
                $customerName === "saha"
                && $isCalledThroughApi
            ) {
                $userData['Name'] = $this->parseGivenName($uniqueId);
                // Duplicate names not allowed; generate new AdjectiveAnimal
                if (!$this->usernameIsUnique($userData['Name'])) {
                    $userData['Name'] = $this->createUsername();
                }
            } else {
                $userData['Name'] = $this->createUsername();
            }
        }
        // the user must have a password or the insert will fail
        $userData['Password'] = $this->generateRandomString();

        $userId = Gdn::userModel()->insertForBasic(
          $userData,
          false,
          ['ValidateSpam' => false]
        );

        if ($isCalledThroughApi) {
            // do not sign in the user;
            // let the controller handle the HTTP response sending instead
            return $userId;
        }

        Gdn::session()->start($userId);
    }

    public function registerMany($users) {
        $userInfo = array();
        foreach ($users as $user) {
            $userData = [];
            $userData['Email'] = $user['email'];
            $userData['Attributes']['UniqueID'] = $user['uniqueId'];
            $userData['Name'] = $user['username'];
            // the user must have a password or the insert will fail
            $userData['Password'] = $this->generateRandomString();
            $userId = Gdn::userModel()->insertForBasic($userData, false, ['ValidateSpam' => false]);

            $d = array('id' => $userId, 'email' => $user['email'], "username" => $user["username"]);
            array_push($userInfo, $d);
        }
        return $userInfo;
    }

    /**
     * Generates a username. The generated username is AdjectiveAnimal, e.g. AmazingPossum.
     * @param $currentUsername
     * @param $firstName
     * @param $lastName
     * @return string
     */
    private function createUsername()
    {
        $path = dirname(__FILE__);
        $pathArr = explode("/", $path);
        // this directory is last element in pathArr
        $dirName = end($pathArr);

        $adjectivesJson = file_get_contents("./plugins/" .$dirName. "/data/adjectives.json");
        $animalsJson = file_get_contents("./plugins/" .$dirName. "/data/animals.json");
        $adjectiveArray = json_decode($adjectivesJson, true);
        $animalArray = json_decode($animalsJson, true);

        $username = $adjectiveArray[mt_rand(0, count($adjectiveArray) - 1)] . $animalArray[mt_rand(0, count($adjectiveArray) - 1)];
        if (!$this->usernameIsUnique($username)) {
            $username = $this->createUniqueUsername($username);
        }

        return $username;
    }


    /**
     * Adds random strings to the end of the given $usernamePrefix, until a unique username is achieved.
     *
     * @param $usernamePrefix
     * @return string
     */
    private function createUniqueUsername($usernamePrefix)
    {
        $usernameLength = c("Garden.User.ValidationLength") ? c("Garden.User.ValidationLength") : "{3,20}"; // 20 is Garden's default max length
        $usernameMaxLength = intval(str_replace("}", "", explode(",", $usernameLength)[1]));
        $randomStringLength = $usernameMaxLength - (strlen($usernamePrefix) + 1); // +1 for the underscore

        if ($randomStringLength < 3) {
            // The plugin settings should make the max length of a username 50 chars, but in case something odd happens...
            $usernamePrefix = substr($usernamePrefix, 0, strlen($usernamePrefix) - 4);
        } else if ($randomStringLength > 10) {
            $randomStringLength = 10;
        }

        $username = $usernamePrefix . '_' . $this->generateRandomString($randomStringLength);

        if (!$this->usernameIsUnique($username)) {
            $username = $this->createUniqueUsername($usernamePrefix); // recursion until a unique name is found
        }

        return $username;
    }


    /**
     * Compares the given $name to the DB and returns true if $name is unique.
     *
     * @param $name
     * @return bool
     */
    private function usernameIsUnique($name)
    {
        return !Gdn::userModel()->SQL->getWhere(
            'User',
            ['Name' => $name]
        )->firstRow(DATASET_TYPE_ARRAY);
    }


    /**
     * Generates a random string for a username.
     *
     * @param int $length
     * @return string
     */
    private function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /**
     * Redirects all /register requests to puheet-auth-ui
     *
     * Note that in theory user registration is also possible via Vanilla's own API using the  POST /applicants endpoint.
     * This registration method is disabled, however, as long as the conf value of Garden.Registration.Method is NOT
     * 'approval'. So, this method is disabled by default, just as it should be.
     */
    public function entryController_register_handler()
    {
        if ($this->externalIamIsEnabled()) {
            $url = c('Plugins.PuheetPlatformIntegration.ExternalSignInUrl');
            header("Location: $url");
            die();
        }

        $url = c('Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl') . '/register';
        if (headers_sent()) {
            die('<script type="text/javascript">window.location=\'' . $url . '\';</script>');
        } else {
            header('Location: ' . $url);
            die();
        }
    }


    /**
     * Sets "now" as the last time the current user was authenticated (without really authenticating them) to prevent
     * edit profile actions from asking the user's Vanilla password.
     */
    public function profileController_beforeEdit_handler()
    {
        Gdn::authenticator()->identity()->setAuthTime(time());
    }


    /**
     * Redirects all /signin requests to puheet-auth-ui
     *
     * Not fail-proof, since it relies on JS. If Vanilla's own sign in button is visible in the UI, a better way to
     * handle this redirect would be to change the button's onclick handler JS inside a theme.
     *
     * Redirect via header doesn't work here.
     */
    public function entryController_signin_handler()
    {
        if ($this->externalIamIsEnabled()) {
            $url = c('Plugins.PuheetPlatformIntegration.ExternalSignInUrl');
        } else {
            $signInUrl = c('Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl') . "/sign-in?target=";
            $target = urlencode("https://" . $_SERVER["HTTP_HOST"] . "/" . $_GET["Target"]);
            $url = $signInUrl . $target;
        }
        die('<script type="text/javascript">window.location=\'' . $url . '\';</script>');
    }


    /**
     * This handles sign out from Puheet platform in case the user signs out using Vanilla's sign out functionality
     * (instead of Puheet navbar).
     */
    public function entryController_signout_handler()
    {
        $this->signOut();
    }


    /**
     * Signs out the user from Vanilla and Puheet platform.
     */
    private function signOut()
    {
        if ($this->externalIamIsEnabled()) {
            $this->externalSignOut();
            die();
        }

        $url = c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl') . "/signout";
        $headers = [];
        $this->request($url, $headers, []);

        // Doesn't matter whether the request succeeded or not
        // Let's just sign out the user from Vanilla and delete session cookies
        $this->removeSessionCookies();

        $frontEndUrl = c('Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl');
        header('Location: ' . $frontEndUrl);
        die();
    }

    private function externalSignOut()
    {
        $this->removeSessionCookies();
        $externalSignOutUrl = c('Plugins.PuheetPlatformIntegration.ExternalSignOutUrl');
        header('Location: ' . $externalSignOutUrl);
    }

    private function removeSessionCookies()
    {
        $secure = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']);
        $domainParts = explode(".", $_SERVER["SERVER_NAME"]);
        $domain = substr($_SERVER["SERVER_NAME"], strlen($domainParts[0]));
        $httpOnly = true;

        setcookie("puheet_info", "", 1, "/", $domain, $secure, $httpOnly);
        setcookie("puheet_session", "", 1, "/", $domain, $secure, $httpOnly);
        unset($_COOKIE["puheet_info"]);
        unset($_COOKIE["puheet_session"]);
    }

    /**
     * Add Javascript to discussion pages.
     *
     * @param discussionController $sender
     */
    public function discussionController_render_before($sender) {
        if (!empty(c('Plugins.PuheetPlatformIntegration.CommentsReloading'))) {
            $sender->addJsFile('comments.js', 'plugins/puheet-platform-integration');
        }
    }


    /**
     * A helper method for making GET and POST requests with curl.
     * @param $url
     * @param array $headers
     * @param null $data
     * @return bool|string
     */
    public function request($url, array $headers, $data = null)
    {
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url); // Set URL on which you want to post the Form and/or data
        if (isset($data)) {
            // Creating a POST call
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        // Should self signed certificates ever be allowed in production?
        if (c('Plugins.PuheetPlatformIntegration.AllowSelfSignedCertificates') == 'TRUE') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Pass TRUE to wait for and catch the response
        curl_setopt($ch, CURLOPT_COOKIE, $_SERVER['HTTP_COOKIE']); // include cookies in the request
        // curl_setopt($ch, CURLOPT_FAILONERROR, 1); // HTTP errors make cURL fail
        curl_setopt($ch, CURLOPT_VERBOSE, 1); // For Debug; shows any error encountered during the operation

        $response = curl_exec($ch);
        $err = curl_error($ch); // echo this to debug

        curl_close($ch);

        return $response;
    }


    /**
     * Saves the plugin's configuration.
     *
     * @return bool|void
     */
    public function setup()
    {
        $this->firstTimeSetup();
        $this->createCustomerAdminRole();

        saveToConfig('Garden.Profile.EditUsernames', true); // TODO maybe some other setting should be enabled as well to prevent name change from breaking the UI?
        saveToConfig('Garden.Profile.EditEmails', false); // disable to prevent users from breaking SSO
        saveToConfig('Garden.User.ValidationLength', '{3,50}');
        saveToConfig('Garden.Registration.ConfirmEmail', false);

        saveToConfig('Cache.Enabled', true);
        saveToConfig('Cache.Method', 'memcached');

        if (!empty(c('Cache.Memcached.Customconf'))) {
            $customConf = c('Cache.Memcached.Customconf');
            saveToConfig('Cache.Memcached.Store', array(0 => $customConf));
        } else {
            saveToConfig('Cache.Memcached.Store', array(0 => 'localhost:11211')); // default Memcached conf
        }

        if (!$this->isConfigured()) {
            // Only set these when the plugin is enabled for the first time
            saveToConfig('Plugins.PuheetPlatformIntegration.ForumSecretKey', '');
            saveToConfig('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl', '');
            saveToConfig('Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl', '');
        }
    }


    /**
     * Retrieves a UniqueID for the admin user if they don't have one set already. This method should be needed only
     * when this plugin is enabled for the first time. After that each user should already have a UniqueID.
     */
    private function firstTimeSetup()
    {
        $user = Gdn::session();

        if (empty($user->User->Attributes["UniqueID"])) {
            // The user who enabled this plugin does not have a UniqueID in Vanilla yet, so one must be retrieved for them.
            $domainParts = explode(".", $_SERVER["SERVER_NAME"], 2);

            //Premium level customer url
            $url = "https://platform." . $domainParts[1] . "/session";

            if (strpos($domainParts[0], "-")) {
                // Basic customer level
                $subdomainParts = explode(".", $domainParts[0], 2);
                $customerName = $subdomainParts[1];
                $url = "https://platform-" . $customerName . "." . $domainParts[1] . "/session";
            }

            $headers = [];
            $userInfoJson = $this->request($url, $headers);
            $userInfo = json_decode($userInfoJson);

            // If the request fails, base_AfterEnablePlugin_handler will disable this plugin.
            if ($userInfo !== false) {
                $uniqueID = $userInfo?->profile?->id;
                if ($uniqueID) {
                    Gdn::userModel()->saveAttribute(
                        $user->UserID,
                        "UniqueID",
                        $uniqueID,
                    );
                    $attributes = $user->getAttributes();
                    $attributes->UniqueID = $uniqueID;
                    Gdn::userModel()->setField(
                        $user->UserID,
                        'Attributes',
                        $attributes,
                    );
                }
            }
        }
    }

    /**
     * Generates a role called "Customer admin" that has fewer permissions than normal admins.
     */
    private function createCustomerAdminRole()
    {
        $roleModel = new RoleModel;
        $role = $roleModel->SQL->getWhere('Role', ['Name' => 'Customer admin'])->firstRow(DATASET_TYPE_ARRAY);

        if (!$role) {

            $roleData = [
                "RoleID" => 0,
                "CanSession" => true,
                "Deletable" => true,
                "Description" => "Customer administrator. Has limited permissions compared to regular admins.",
                "Name" => "Customer admin",
                "Permissions" => [
                    "id" => 1,
                    "Garden.Email.View" => 1,
                    "Garden.Settings.Manage" => 0,
                    "Garden.Settings.View" => 1,
                    "Garden.SignIn.Allow" => 1,
                    "Garden.Users.Add" => 0,
                    "Garden.Users.Edit" => 1,
                    "Garden.Users.Delete" => 0,
                    "Garden.Users.Approve" => 0,
                    "Garden.Activity.Delete" => 1,
                    "Garden.Activity.View" => 1,
                    "Garden.Profiles.View" => 1,
                    "Garden.Profiles.Edit" => 1,
                    "Garden.Curation.Manage" => 1,
                    "Garden.Moderation.Manage" => 1,
                    "Garden.PersonalInfo.View" => 0,
                    "Garden.AdvancedNotifications.Allow" => 1,
                    "Garden.Community.Manage" => 1,
                    "Garden.Tokens.Add" => 0,
                    "Garden.Uploads.Add" => 1,
                    "Vanilla.Tagging.Add" => 1,
                    "Conversations.Moderation.Manage" => 0,
                    "Conversations.Conversations.Add" => 1,
                    "Vanilla.Approval.Require" => 0,
                    "Vanilla.Comments.Me" => 0,
                    "Vanilla.Discussions.View" => 1,
                    "Vanilla.Discussions.Add" => 1,
                    "Vanilla.Discussions.Edit" => 1,
                    "Vanilla.Discussions.Announce" => 1,
                    "Vanilla.Discussions.Sink" => 1,
                    "Vanilla.Discussions.Close" => 1,
                    "Vanilla.Discussions.Delete" => 1,
                    "Vanilla.Comments.Add" => 1,
                    "Vanilla.Comments.Edit" => 1,
                    "Vanilla.Comments.Delete" => 1,
                    "Plugins.DiscussionPolls.Add" => 1,
                    "Plugins.DiscussionPolls.View" => 1,
                    "Plugins.DiscussionPolls.Vote" => 1,
                    "Plugins.DiscussionPolls.Manage" => 1,
                    "Type" => "global",
                    "Category" => [ 0 => [
                        "Vanilla.Discussions.View" => 1,
                        "Vanilla.Discussions.Add" => 1,
                        "Vanilla.Discussions.Edit" => 1,
                        "Vanilla.Discussions.Announce" => 1,
                        "Vanilla.Discussions.Sink" => 1,
                        "Vanilla.Discussions.Close" => 1,
                        "Vanilla.Discussions.Delete" => 1,
                        "Vanilla.Comments.Add" => 1,
                        "Vanilla.Comments.Edit" => 1,
                        "Vanilla.Comments.Delete" => 1,
                        "CategoryID" => -1
                        ]
                    ]
                ],
                "PersonalInfo" => false,
                "Type" => "administrator"
            ];

            $roleModel->save($roleData, ["DoPermissions" => true]);
        }
    }

    public function base_render_after($asd, $qwe)
    {
        $headers = headers_list(); // get list of headers
        foreach ($headers as $index => $value) {
            list($key, $value) = explode(': ', $value);

            unset($headers[$index]);

            $headers[$key] = $value;
        }

        $header = $headers['Content-Type'];

        if (($header == "application/json; charset=utf-8") == false) {
            $this->loadNavbar();
        }
    }

    private function loadNavbar()
    {
        $navbarUrl = c('Plugins.PuheetPlatformIntegration.PuheetPlatformNavbarUrl');
        if (empty($navbarUrl)) return;

        echo "
        <style>.navbar { position: inherit !important; }</style>
        <script id=\"puheet-remove\">
            if (typeof puheetNavbarCalled === 'undefined') {
                var puheetNavbarCalled = true;

                function docReady(fn) {
                    // see if DOM is already available
                    if (document.readyState === 'complete' || document.readyState === 'interactive') {
                        setTimeout(fn, 1); // call on next available tick
                    } else {
                        document.addEventListener('DOMContentLoaded', fn);
                    }
                }

                function getNavbar() {
                    var request = new XMLHttpRequest();

                    request.open('GET', '$navbarUrl', true);

                    request.onload = function() {
                        if (request.status >= 200 && request.status < 400) {
                            var resp = request.responseText;

                            var fragment = document.createRange().createContextualFragment(resp);
                            docReady(() => { document.querySelector('body').prepend(fragment); window.scrollTo(0, 0); });
                            var scriptElement = document.getElementById('puheet-remove');
                            scriptElement.parentNode.removeChild(scriptElement);
                        }
                    };

                    request.send();
                }
                getNavbar();
            }
        </script>
        ";
    }


    /**
     * Disables the plugin just after it has been enabled, if the user doesn't have a UniqueID. If the plugin would stay
     * enabled the user would be logged out and then we'd have no admin user.
     *
     * @throws Exception
     */
    public function base_AfterEnablePlugin_handler()
    {
        $user = Gdn::session();

        if (strpos($_SERVER["PATH_INFO"], "puheet-platform-integration") !== false
            && empty($user->getAttributes()["UniqueID"])) {
            Gdn::pluginManager()->disablePlugin("puheet-platform-integration");
            Gdn::controller()->informMessage("Puheet Platform Integration DISABLED. Reason: could not retrieve your UniqueID from the DB or identity management system");
        }
    }


    /**
     * Removes plug-in configurations on disable.
     */
    public function onDisable()
    {
        saveToConfig('Garden.Profile.EditUsernames', false);
        saveToConfig('Garden.Profile.EditEmails', true); // the default setting
        saveToConfig('Garden.User.ValidationLength', '{3,20}');

        saveToConfig('Cache.Enabled', false);
        saveToConfig('Cache.Method', false);
        saveToConfig('Cache.Memcached.Store', false);

        // These settings could be removed here, but having to re-enter these configs
        // gets annoying pretty fast when you toggle the plugin off and on
        // saveToConfig('Plugins.PuheetPlatformIntegration.ForumSecretKey', '');
        // saveToConfig('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl', '');
        // saveToConfig('Plugins.PuheetPlatformIntegration.PuheetPlatformNavbarUrl', '');
    }

    /**
     * Tells whether this add-on is configured properly.
     *
     * @return bool
     */
    public function isConfigured()
    {
        return !empty(c('Plugins.PuheetPlatformIntegration.ForumSecretKey'))
            && !empty(c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl'))
            && !empty(c('Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl'))
            && (
                empty(c('Plugins.PuheetPlatformIntegration.ExternalIAM')) ||
                (
                    !empty('Plugins.PuheetPlatformIntegration.ExternalSignInUrl') &&
                    !empty('Plugins.PuheetPlatformIntegration.ExternalSignOutUrl')
                )
            );
        // && !empty(c('Plugins.PuheetPlatformIntegration.PuheetPlatformNavbarUrl')); // don't force using the navbar here
    }


    public function externalIamIsEnabled()
    {
        return !!(c('Plugins.PuheetPlatformIntegration.ExternalIAM')) &&
            !empty('Plugins.PuheetPlatformIntegration.ExternalSignInUrl') &&
            !empty('Plugins.PuheetPlatformIntegration.ExternalSignOutUrl');
    }

    /**
     * Endpoint for configuring this add-on.
     *
     * @param socialController $sender
     * @param array $args
     */
    public function settingsController_puheetplatformintegration_create($sender)
    {
      $sender->permission('Garden.Settings.Manage');
      if ($sender->Form->authenticatedPostBack()) {
        $settings = [
          'Plugins.PuheetPlatformIntegration.CustomerName' => trim(
            $sender->Form->getFormValue('CustomerName')
          ),
          'Plugins.PuheetPlatformIntegration.ForumSecretKey' => trim(
            $sender->Form->getFormValue('ForumSecretKey')
          ),
          'Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl' => trim(
            $sender->Form->getFormValue('PuheetPlatformBackendUrl')
          ),
          'Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl' => trim(
            $sender->Form->getFormValue('PuheetPlatformFrontendUrl')
          ),
          'Plugins.PuheetPlatformIntegration.PuheetPlatformNavbarUrl' => trim(
            $sender->Form->getFormValue('PuheetPlatformNavbarUrl')
          ),
          'Plugins.PuheetPlatformIntegration.CommentsReloading' => trim(
            $sender->Form->getFormValue('CommentsReloading')
          ),
          'Plugins.PuheetPlatformIntegration.ExternalIAM' => trim(
            $sender->Form->getFormValue('ExternalIAM')
          ),
          'Plugins.PuheetPlatformIntegration.ExternalSignInUrl' => trim(
            $sender->Form->getFormValue('ExternalSignInUrl')
          ),
          'Plugins.PuheetPlatformIntegration.ExternalSignOutUrl' => trim(
            $sender->Form->getFormValue('ExternalSignOutUrl')
          ),
          // In use or not? Related to saha-sync?
          'Plugins.PuheetPlatformIntegration.ActionsApiUrl' => trim(
            $sender->Form->getFormValue('ActionsApiUrl')
          ),
        ];

        saveToConfig($settings);
        $sender->informMessage(t("Your settings have been saved."));

      } else {
        $sender->Form->setValue(
          'CustomerName',
          c('Plugins.PuheetPlatformIntegration.CustomerName')
        );
        $sender->Form->setValue(
          'ForumSecretKey',
          c('Plugins.PuheetPlatformIntegration.ForumSecretKey')
        );
        $sender->Form->setValue(
          'PuheetPlatformBackendUrl',
          c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl')
        );
        $sender->Form->setValue(
          'PuheetPlatformFrontendUrl',
          c('Plugins.PuheetPlatformIntegration.PuheetPlatformFrontendUrl')
        );
        $sender->Form->setValue(
          'PuheetPlatformNavbarUrl',
          c('Plugins.PuheetPlatformIntegration.PuheetPlatformNavbarUrl')
        );
        $sender->Form->setValue(
          'CommentsReloading',
          c('Plugins.PuheetPlatformIntegration.CommentsReloading')
        );
        $sender->Form->setValue(
          'ExternalIAM',
          c('Plugins.PuheetPlatformIntegration.ExternalIAM')
        );
        $sender->Form->setValue(
          'ExternalSignInUrl',
          c('Plugins.PuheetPlatformIntegration.ExternalSignInUrl')
        );
        $sender->Form->setValue(
          'ExternalSignOutUrl',
          c('Plugins.PuheetPlatformIntegration.ExternalSignOutUrl')
        );
        // In use or not? Related to saha-sync?
        $sender->Form->setValue(
          'ActionsApiUrl',
          c('Plugins.PuheetPlatformIntegration.ActionsApiUrl')
        );
      }

      $sender->setHighlightRoute('dashboard/settings');
      $sender->setData('Title', t('Puheet Platform Integration Settings'));
      $sender->render('Settings', '', 'plugins/puheet-platform-integration');
    }
}
