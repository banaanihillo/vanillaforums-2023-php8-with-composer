# Puheet Platform Integration (Vanilla)
This plugin integrates Vanilla Forums with Puheet Platform, affecting Vanilla's sign in, sign out, register and session handling.

# Installation
- Download this plugin to <vanilla>/plugins.

- Install Memcached Server and PHP Module:
  ```
  sudo apt install -y memcached php-memcached
  sudo service nginx restart
  sudo service php7.2-fpm restart
  ```
  The config file for Memcached is installed to /etc/memcached.conf by default. Edit as needed.
  ```
  
- Enable caching by replacing the default caching configuration with the following (inside `<vanilla>/conf/config.php`):
 ```
// Cache
$Configuration['Cache']['Enabled'] = true;
$Configuration['Cache']['Method'] = 'memcached';
$Configuration['Cache']['Memcached']['Store'] = array (
  0 => 'localhost:11211',
);
 ```

- The plugin assumes Memcached can be accessed from localhost:11211. If this is not the case, add the following line to `<vanilla>/conf/config.php` manually:
```
$Configuration['Cache']['Memcached']['Customconf'] = array ( 0 => 'your conf here' );
```
 
 - Make a Management API client to OnePortal with the right to view user accounts and IDs (two rights).
 
- If you want to configure this plugin via a script copy the following rows with your custom values to `<vanilla>/conf/config.php`:
  ```
// Plugins
$Configuration['EnabledPlugins']['puheet-platform-integration'] = true;

$Configuration['Plugins']['PuheetPlatformIntegration']['ForumSecretKey'] = 'YourSecretKey'; // for authenticating with Puheet Platform Backend
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformBackendUrl'] = 'https://community.omakone.local/api'; // Platform Backend URL
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformFrontendUrl'] = 'https://community.omakone.local/auth'; // Sign in UI URL (Puheet Auth)
// $Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformIdentityManagementSystem'] = 'https://id.puheet.com'; // Identity management system URL (?)
// $Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformIdentityUsername'] = 'asdasd'; // IMS Management API key (?)
// $Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformIdentityPassword'] = 'qweqwe'; // IMS Management API secret (?)
$Configuration['Plugins']['PuheetPlatformIntegration']['PuheetPlatformNavbarUrl'] = 'https://community.omakone.local/nav'; // navbar URL, can be left empty if you don't want to load the navbar to the UI
$Configuration['Plugins']['PuheetPlatformIntegration']['CommentsReloading'] = '0'; // whether to enable showing a message about new comments
$Configuration['Plugins']['PuheetPlatformIntegration']['ExternalIAM'] = '0'; // whether to enable external IAM ('0' or '1')
$Configuration['Plugins']['PuheetPlatformIntegration']['ExternalSignInUrl'] = 'https://customer.fi/login?target=https://forum-customer.puheet.info'; // External customer's sign in URL
$Configuration['Plugins']['PuheetPlatformIntegration']['ExternalSignOutUrl'] = 'https://customer.fi/logout'; // External customer's sign out URL
```
