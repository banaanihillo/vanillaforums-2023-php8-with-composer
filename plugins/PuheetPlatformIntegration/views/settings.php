<?php if (!defined('APPLICATION')) exit();
?>
    <h1><?php echo $this->data('Title'); ?></h1>

    <div class="padded">
        <?php echo t('Integrate this forum with Puheet Platform. Note that this plugin will work only after it has been configured and the user enabling it has a UniqueID saved to the DB from the IMS.'); ?>
    </div>
<?php
echo $this->Form->open();
echo $this->Form->errors();
?>
    <ul>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('Forum Secret Key <div class="info description">Used for authenticating Puheet Backend.</div>', 'ForumSecretKey');
            echo $this->Form->textBoxWrap('ForumSecretKey');
            ?>
        </li>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('Puheet Platform Backend URL', 'PuheetPlatformBackendUrl');
            echo $this->Form->textBoxWrap('PuheetPlatformBackendUrl');
            ?>
        </li>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('Puheet Platform Frontend URL <div class="info description">Puheet Auth UI.</div>', 'PuheetPlatformFrontendUrl');
            echo $this->Form->textBoxWrap('PuheetPlatformFrontendUrl');
            ?>
        </li>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('Puheet Navbar URL <div class="info description">Leave this empty if you do not wish to load the navbar to the UI.</div>', 'PuheetPlatformNavbarUrl');
            echo $this->Form->textBoxWrap('PuheetPlatformNavbarUrl');
            ?>
        </li>
        <li class="form-group">
            <div class="label-wrap">
                <?php echo $this->Form->label('Comments reloading <div class="info description">Enable to show a "refresh page" button under the reply textarea and a message when new comments are available.</div>', 'CommentsReloading'); ?>
            </div>
            <div class="input-wrap">
                <?php echo $this->Form->checkBox('CommentsReloading'); ?>
            </div>
        </li>
        <li class="form-group">
            <div class="label-wrap">
                <?php echo $this->Form->label('External IAM <div class="info description">Use an external IAM system with Puheet backend. The system must be separately configured with Puheet backend for it to work.</div>', 'ExternalIAM'); ?>
            </div>
            <div class="input-wrap">
                <?php echo $this->Form->checkBox('ExternalIAM'); ?>
            </div>
        </li>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('External Sign In URL <div class="info description">Leave this empty if you do not use external IAM authentication.</div>', 'ExternalSignInUrl');
            echo $this->Form->textBoxWrap('ExternalSignInUrl');
            ?>
        </li>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('External Sign Out URL <div class="info description">Leave this empty if you do not use external IAM authentication.</div>', 'ExternalSignOutUrl');
            echo $this->Form->textBoxWrap('ExternalSignOutUrl');
            ?>
        </li>
        <li class="form-group">
            <?php
            echo $this->Form->labelWrap('Actions-API URL <div class="info description">You can leave this empty if this community is not using actions API.</div>', 'ActionsApiUrl');
            echo $this->Form->textBoxWrap('ActionsApiUrl');
            ?>
        </li>
    </ul>
<?php echo $this->Form->close('Save');
