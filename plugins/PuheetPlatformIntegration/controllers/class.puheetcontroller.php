<?php
/**
 * Handles user authentication and registration.
 *
 * @copyright Puheet.com
 */

/**
 * Handles the /puheet endpoint.
 */
class PuheetController extends Gdn_Controller
{


    /**
     * PuheetController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setHeader('Cache-Control', \Vanilla\Web\CacheControlMiddleware::NO_CACHE);
    }


    /**
     * Ends the session of the signed out user.
     */
    public function signOut() {

        if (Gdn::session()->UserID) {
            Gdn::session()->end();
        }

        return $this->httpResponse(200);
    }


    /**
     * Endpoint for custom user registration. Called by puheet-platform-backend after a user has been created in the
     * main database (currently OnePortal).
     *
     */
    public function register()
    {
        if (!$this->hasValidCredentials()) {
            return $this->httpResponse(401);
        }

        $arguments = $this->Request->getRequestArguments("post");

        if (!$this->Request->isPostBack() || !isset($arguments["email"]) || !isset($arguments["uniqueId"])) {
            return $this->httpResponse(400);
        }

        $id = PuheetPlatformIntegrationPlugin::instance()->register($arguments["email"], $arguments["uniqueId"], true);

        if (!$id) {
            return $this->httpResponse(
                500,
                'Creating a new user failed. Possible reasons: invalid request fields, duplicate email or username.'
            ); // not the best error message, but $id doesn't tell us much
        }

        return $this->httpResponse(201);
    }

    /**
     * Returns true or false depending on whether the current user is an admin.
     */
    public function getAdminLevel()
    {
        $arguments = $this->Request->getRequestArguments("post");

        if (!$this->hasValidCredentials() || !$this->Request->isPostBack() || !isset($arguments["email"])) {
            return $this->httpResponse(200, ["admin" => false]);
        }

        $vanillaUser = Gdn::userModel()->getWhere(['Email' => $arguments["email"]])->resultArray();

        if (count($vanillaUser) !== 1) {
            return $this->httpResponse(200, ["admin" => false]);
        }

        $userLevels = Gdn::sql()
            ->select('r.Name')
            ->from('UserRole ur')
            ->join('Role r', 'ur.RoleID = r.RoleID', 'inner')
            ->where('ur.UserID', $vanillaUser[0]["UserID"])
            ->beginWhereGroup()
            ->where('r.Name', 'Administrator')
            ->orWhere('r.Name', 'Customer admin')
            ->orWhere('r.Name', 'Moderator')
            ->endWhereGroup()
            ->get()->resultArray();

        return $this->httpResponse(200, $userLevels);
    }


    /**
     * Gets a user's (identified by email) unread message and notification counts.
     * @return mixed
     */
    public function getCounts() {
        $arguments = $this->Request->getRequestArguments("post");

        if (!$this->hasValidCredentials() || !$this->Request->isPostBack() || !isset($arguments["email"])) {
            return $this->httpResponse(400, "Bad request or credentials");
        }

        $vanillaUser = Gdn::userModel()->getWhere(['Email' => $arguments["email"]])->resultArray();

        if (count($vanillaUser) !== 1) {
            return $this->httpResponse(500, "Found more than one user with the given email.");
        }

        return $this->httpResponse(200, [
            "messages" => $vanillaUser[0]["CountUnreadConversations"],
            "notifications" => $vanillaUser[0]["CountNotifications"]
        ]);

    }


    /**
     * Finds and returns a user's profile pic URL and username by email.
     */
    public function getUserProfile() {
        $arguments = $this->Request->getRequestArguments("post");

        if (!$this->hasValidCredentials()) {
            return $this->httpResponse(401);
        }

        if (!$this->Request->isPostBack() || !isset($arguments["email"])) {
            return $this->httpResponse(400);
        }

        $userInfo = Gdn::userModel()->getByEmail($arguments["email"]);

        if ($userInfo === false) {
            return $this->httpResponse(404, "User not found");
        }

        $username = $userInfo->Name;
        $photoUrl = $userInfo->Photo;

        if (isset($photoUrl)) {
            /* An example of a saved photoUrl: userpics/758/H50ZR83X6R7N.jpg
             * The URL doesn't point to a picture. It's the base URL to which either a p (bigger size)
             * or an n (smaller size) must be inserted to get the actual photo URL:
             * userpics/758/nH50ZR83X6R7N.jpg or userpics/758/pH50ZR83X6R7N.jpg */
            $lastSlash = strrpos($photoUrl, '/');
            $photoUrl = substr_replace($photoUrl, "n", $lastSlash + 1, 0);
            // photoUrl changed to support new domain schema
            // if upgrading this plugin with old domain schema, this will broke
            $photoUrl = "https://" . $_SERVER["HTTP_HOST"] . "/forum/uploads/$photoUrl";
        } else {
            $photoUrl = Gdn::userModel()->getDefaultAvatarUrl($userInfo);

            if (strpos($photoUrl, '//') === false) {
                // Users' custom pics are saved on Vanilla's server without the server's domain and protocol
                $photoUrl = "https://" . $_SERVER["HTTP_HOST"] . $photoUrl;
            } else if (strpos($photoUrl, 'https') === false) {
                // Some pic URLs are stored without the protocol, we'll always use HTTPS
                $photoUrl = "https:" . $photoUrl;
            }
        }

        return $this->httpResponse(200, ["username" => $username, "profilePicUrl" => $photoUrl]);
    }

    /**
     * Deletes the given user ("delete by email") and leaves their comments untouched.
     *
     * @return mixed
     */
    public function deleteUser() {
        $arguments = $this->Request->getRequestArguments("post");

        if (!$this->hasValidCredentials()) {
            return $this->httpResponse(401);
        }

        if (!$this->Request->isPostBack() || !isset($arguments["email"])) {
            return $this->httpResponse(400);
        }

        $userInfo = Gdn::userModel()->getByEmail($arguments["email"]);

        if ($userInfo === false) {
            return $this->httpResponse(404, "User not found");
        }

        $deleteSuccess = Gdn::userModel()->deleteID($userInfo->UserID, ["DeleteMethod" => "keep"]);

        if (!$deleteSuccess) {
            return $this->httpResponse(500, "Could not delete " . $arguments["email"]);
        }

        return $this->httpResponse(200);
    }


    /**
     * This API uses Basic Authentication. This function returns true if the request's username equals
     * the main domain name of the customer. E.g. in the case of "forum.example.com" a valid username would
     * be "example". Tha API key value is retrieved from this plugin's settings.
     */
    private function hasValidCredentials()
    {
        $user = $this->Request->getRequestArguments("server")["PHP_AUTH_USER"];
        $apiKey = $this->Request->getRequestArguments("server")["PHP_AUTH_PW"];

        $forumSubdomain = explode(".", $_SERVER["HTTP_HOST"])[0];
        if (strpos($forumSubdomain, "-") !== false) {
            $customerUser = explode("-", $forumSubdomain)[1];
        } else {
            $customerUser = explode(".", $_SERVER["HTTP_HOST"])[1];
        }


        $customerApiKey = c('Plugins.PuheetPlatformIntegration.ForumSecretKey');

        return $user === $customerUser && $apiKey === $customerApiKey;

    }

    /**
     * Returns the timestamp of the comment that was edited (=inserted, deleted or updated) last.
     *
     * @param $discussionId - int, e.g. 5
     * @return mixed
     */
    public function getTimeOfNewestComment($discussionId, $commentId) {
        if (!Gdn::session()->UserID) {
            return $this->httpResponse(401);
        }

        if ((!$discussionId || !is_numeric($discussionId)) && (!$commentId || !is_numeric($commentId))) {
            return $this->httpResponse(400, 'Required query param missing: discussionId (numeric) or commentId (numeric)');
        }

        try {
            if (!$discussionId) {
                $discussionId = $this->getDiscussionIdByCommentId($commentId);
            }

            $newestComment = $this->getNewestCommentTimestampByDiscussionId($discussionId);

            if (!$newestComment) {
                $responseData = [];
                $responseData["isOwn"] = false;
                $responseData["timestamp"] = false;
                return $this->httpResponse(200, $responseData, "latestComment");
            }

            // Parsing a timestamp
            $newestCommentTime = $newestComment["NewestDate"];
            $newestCommentTimestamp = new DateTime($newestCommentTime);

            // Checking if the latest comment belongs to the current user
            $newestCommentIsOwn = $newestComment["InsertUserID"] === Gdn::session()->UserID;

            // Creating a response "object"
            $responseData = [];
            $responseData["isOwn"] = $newestCommentIsOwn;
            $responseData["timestamp"] = $newestCommentTimestamp;

            return $this->httpResponse(200, $responseData, "latestComment");
        } catch (Exception $e) {
            return $this->httpResponse(500);
        }
    }

    /**
     * Returns the timestamp of the latest comment to the given discussion.
     *
     * @param $discussionId
     * @return mixed
     * @throws Exception
     */
    private function getNewestCommentTimestampByDiscussionId($discussionId) {
        $newestComment = Gdn::sql()
            ->select('GREATEST(DateInserted, COALESCE(DateDeleted, 0), COALESCE(DateUpdated, 0)) as NewestDate, InsertUserID')
            ->from('Comment')
            ->where('DiscussionID', $discussionId)
            ->orderBy('NewestDate', 'DESC')
            ->limit(1)
            ->get()->resultArray();

        return $newestComment[0];
    }

    /**
     * Returns the ID of the discussion the comment belongs to.
     * @param $commentId
     * @return mixed
     * @throws Exception
     */
    private function getDiscussionIdByCommentId($commentId) {
        $discussionId = Gdn::sql()
            ->select('DiscussionID')
            ->from('Comment')
            ->where('CommentID', $commentId)
            ->limit(1)
            ->get()->resultArray();

        return $discussionId[0]["DiscussionID"];
    }


    /**
     * Handy HTTP JSON response function.
     *
     * @param $statusCode
     * @param string $message
     * @return mixed
     */
    private function httpResponse($statusCode, $message = "", $key = "message")
    {
        $return = [];

        if (($statusCode < 200 || $statusCode >= 400) && !empty($message)) {
            $return["error"] = $message;
        } else if (!empty($message)) {
            $return[$key] = $message;
        }

        http_response_code($statusCode);
        header('Content-type: application/json');

        if (!empty($return)) {
            return print_r(json_encode($return));
        }
    }

}
