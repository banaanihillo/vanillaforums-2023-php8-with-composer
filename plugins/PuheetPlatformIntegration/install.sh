#!/bin/bash
set -e

sudo rm -rf /var/www/forum/plugins/PuheetPlatformIntegration/
sudo mkdir /var/www/forum/plugins/PuheetPlatformIntegration/
sudo cp -r * /var/www/forum/plugins/PuheetPlatformIntegration/

sudo chown -R www-data:www-data /var/www/forum
# Required for puppeteer preview images to work
sudo chown -R ubuntu:ubuntu /var/www/forum/uploads/images
sudo rm -rf /var/www/forum/cache/*
sudo service nginx reload
