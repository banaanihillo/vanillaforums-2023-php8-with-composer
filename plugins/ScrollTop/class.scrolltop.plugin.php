<?php if (!defined('APPLICATION')) exit();
/* 
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, either version 3 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  homepage: "https://www.planamigo.org" 
 */

class ScrollTopPlugin extends Gdn_Plugin {

	  // runs once every page load
	  public function __construct() {
		parent::__construct();
	  }

	  //This is a common hook that fires for all controllers on the Render method
	  public function assetModel_styleCss_handler($sender) {
		// Do not show on the dashboard and mobile
		// add && !C('EnabledPlugins.embedvanilla', FALSE) to not show on embedded forums 
	      //if(!IsMobile() && $sender->MasterView != 'admin') {
		  // add CSS
	          $sender->addCSSFile('scrolltop.css', 'plugins/ScrollTop');
		  //}
	  }
  
	  public function Base_AfterBody_handler($sender) {
		// Do not show on the dashboard and mobile
		// add && !C('EnabledPlugins.embedvanilla', FALSE) to not show on embedded forums 
	      //if(!IsMobile() && $sender->MasterView != 'admin') {
			// echo div with a little bit of javascript
			echo '<div id="scroller" class="btop" style="display: none;"><span class="btop-but">'.T('top').'</span></div>
			<script>
				document.addEventListener("DOMContentLoaded", function() {
					var scroller = document.getElementById("scroller");

					window.addEventListener("scroll", function() {
						if (window.scrollY > 0) {
							scroller.style.display = "block";
						} else {
							scroller.style.display = "none";
						}
					});

					scroller.addEventListener("click", function() {
						// Below code scrolls to the top without animation
						// document.body.scrollTop = 0;
						// document.documentElement.scrollTop = 0;
						document.documentElement.scrollIntoView({behavior:"smooth"});
					});
				});
			</script>';
		 //}
	 }
}
